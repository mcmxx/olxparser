import { toInteger } from "lodash";
import {
    HeadingModel,
    OffersModel
} from "../src/models";
import OfferService from "../src/services/OfferService";

const CHUNK_SIZE = 500;

console.log("Headings clean routine");

(async () => {
    console.log("Processing offers get all cities");
    const headingStringsSet = new Set();
    const offersNumber = await OfferService.getAllOffersNumber();
    const pages = toInteger(offersNumber / CHUNK_SIZE);
    console.log("Find cities for Transport");
    for(let i = 0; i < pages; i++) {
        console.log("Page " + i + " of " + pages);
        const offersList = await OffersModel
            .paginate(
                null,
                {
                    limit: CHUNK_SIZE,
                    offset: CHUNK_SIZE * i,
                });
        for(let j = 0; j < offersList.docs.length; j++) {
            const offer = offersList.docs[j];
            const regExp = /(Транспорт)+\s+([А-Яа-я]+)/g;
            const found = regExp.exec(offer.headingString);
            let headingString = null;
            if(found) {
                headingString = offer.headingString.replace(new RegExp(found[2], "gi"), '');
            } else {
                const regExp = /(Работа)+\s+([А-Яа-я]+)/g;
                const found = regExp.exec(offer.headingString);
                if(found) {
                    headingString = offer.headingString.replace(new RegExp(found[2], "gi"), '');
                } else {
                    const regExp = /(Животные)+\s+([А-Яа-я]+)/g;
                    const found = regExp.exec(offer.headingString);
                    if(found) {
                        headingString = offer.headingString.replace(new RegExp(found[2], "gi"), '');
                    } else {
                        headingString = offer.headingString;
                    }
                }
            }
            const slashRegexp = /((\s|\t|\r|\n)*\/(\s|\t|\r|\n)*)+/g;
            headingString = headingString.replace(slashRegexp, '/');
            if(headingString.trim() !== '')  headingStringsSet.add(headingString.trim());
            if(offer.headingString !== headingString) {
                await OffersModel.updateMany({_id: offer._id}, {headingString}).exec();
            }
        }
    }


    console.log("Processing headings");
    await HeadingModel.deleteMany({}).exec();
    for(let heading of headingStringsSet) {
        console.log("Add heading: " + heading);
        const headingModel = new HeadingModel({ heading, createdAt: new Date() });
        const saveResult = await headingModel.save();
        const headingId = saveResult._id;
        await OffersModel.updateMany({headingString: heading}, {headingId}).exec();
    }

})();



