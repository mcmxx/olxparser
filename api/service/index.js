import ScheduleService from "../src/services/ScheduleService";
import OfferService from "../src/services/OfferService";
import OfferDetailService from "../src/services/OfferDetailService";
import config from "../src/config";

const exportOffersToCCJob = ScheduleService
    .addJob(
        "Export offers to Call center",
        '*/5 * * * *',
        async function () { await OfferService.exportToCallCenter() }
    );
const syncDetailJob = ScheduleService
    .addJob(
        "Sync details",
        '*/30 * * * *',
        OfferDetailService.makeOfferDetailCollection
    )
if(!config.callCenter.enableExport) {
    ScheduleService.cancelJob(exportOffersToCCJob);
}
if(!config.main.enableSyncDetails) {
    ScheduleService.cancelJob(syncDetailJob);
}

