export default {
    main: {
        user: 'admin',
        password: 'adminka',
        enableSyncDetails: false
    },
    mongodb: {
        host: 'mongo',
        //host: '127.0.0.1',
        port: '27017',
        dbname: 'olxparser',
        user: 'olxparser',
        password: 'olxparser'
    },
    callCenter: {
        token: "azWsylP7od0XAgXfenVPyMwWYvWwcO3o",
        onceImportNumber: 500,
        enableExport: false
    },
    /*obyavadb: {
        host: '62.149.23.73',
        port: '3306',
        dbname: 'obyava',
        user: 'parseruser',
        password: 'ggrdAirlcyaaTZqs',
        dialect: 'mysql'
    }*/
    obyavadb: {
        host: 'mysql',
        port: '3306',
        dbname: 'obyava',
        user: 'obyava',
        password: 'obyava',
        dialect: 'mysql'
    }
}