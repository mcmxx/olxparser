import Controller, { VERB } from "../core/Controller";
import { PROXY_URL } from "../constants/urls";
import { ProxyModel } from "../models";
import Error from "../core/Error";

class ProxyController extends Controller {
    get routes() {
        return [
            {
                route: PROXY_URL,
                verb: VERB.GET,
                handler: this.getProxies
            },
            {
                route: `${PROXY_URL}/:id`,
                verb: VERB.GET,
                handler: this.getProxy
            },
            {
                route: PROXY_URL,
                verb: VERB.POST,
                handler: this.createProxy
            },
            {
                route: `${PROXY_URL}/:id/switch`,
                verb: VERB.PUT,
                handler: this.switchProxy
            },
            {
                route: `${PROXY_URL}/:id`,
                verb: VERB.PUT,
                handler: this.updateProxy
            },
            {
                route: `${PROXY_URL}/:id`,
                verb: VERB.DELETE,
                handler: this.deleteProxy
            }
        ];
    }

    /**
     * @api {get} /proxies getAllProxies
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {Number} limit
     * @apiParam {Number} offset
     * @apiParam {String} [search] search string
     * @apiParam {String} [order] order direction 'asc' or 'desc'
     * @apiParam {String} [orderBy] order by field. url, port
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success",
     *      "items": [
     *      {
     *          "_id": "dfsfrwe4534345",
     *          "url": "192.168.2.105",
     *          "port": "1081",
     *          "username": "test",
     *          "password": "test",
     *          "__v": 0
     *      },
     *      {
     *          "_id": "djnjwerw4j45w4n5w4n",
     *          "url": "192.168.2.105",
     *          "port": "1080",
     *          "username": "test",
     *          "password": "test",
     *          "__v": 0
     *      }
     *      ],
     *      "total": 2
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     */
    async getProxies(req, res, next) {
        const {
            query: {
                limit ,
                offset,
                search,
                order,
                orderBy
            }
        } = req;

        const queryOrderBy = orderBy === '' ? 'url' : orderBy;
        const queryOrder = order === '' ? 'asc' : order;

        let query;
        if(search && search.trim()) {
            const regexp = new RegExp(search.trim(), 'i');
            query = {
                $or: [
                    {
                        url: regexp
                    },
                    {
                        port: regexp
                    }
                ]
            }
        } else {
            query = {};
        }

        let proxies = null;
        let total = 0;
        
        try {
            proxies = await ProxyModel.paginate(
                query,
                {
                    limit,
                    offset,
                    sort: [
                        [queryOrderBy, queryOrder]
                    ]
                }
            );

            total = await ProxyModel
                .countDocuments(query)
                .exec();
        } catch (e) {
            console.log(e);
            next(e);
        }

        return res.json({
            status: 'success',
            items: proxies.docs === null ? [] : proxies.docs,
            total
        });
    }

    /**
     * @api {get} /proxies/:id getProxy
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {String} id
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success",
     *      "item": {
     *          "_id": "dfsfrwe4534345",
     *          "url": "192.168.2.105",
     *          "port": "1081",
     *          "username": "test",
     *          "password": "test",
     *          "__v": 0
     *      }
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 404 Not Found
     * {
     *      "status": 404,
     *      "errors": "Not found"
     * }
     *
     */
    async getProxy(req, res, next) {
        const { id } = req.params;
        let proxy;

        try {
            proxy = await ProxyModel
                .findOne({_id: id})
                .exec();
        } catch (e) {
            console.log(e);
            return next(new Error("Not found", 404));
        }

        if(!proxy) {
            return next(new Error("Not found", 404));
        }

        return res.json({
            status: 'success',
            item: proxy
        })
    }

    /**
     * @api {post} /proxies createProxy
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {String} url
     * @apiParam {String} [port]
     * @apiParam {String} [username]
     * @apiParam {String} [password]
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     * @apiParamExample {json} Request-Example:
     * {
     *      "url": "192.168.2.105",
     *      "port": "1081",
     *      "username": "test",
     *      "password": "test"
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success"
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "status": 400,
     *     "errors": "Invalid params"
     * }
     *
     */
    async createProxy(req, res, next) {
        const proxy = req.body;

        if(!proxy.url || proxy.url.trim() === '') {
            return next(new Error("Invalid params"));
        }

        const proxyModel = new ProxyModel(proxy);
        
        try {
            await proxyModel.save();
        } catch (e) {
            console.log(e);
            return next(e);
        }

        return res.json({status: 'success'});
    }

    /**
     * @api {post} /proxies/:id updateProxy
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {String} id
     * @apiParam {String} [url]
     * @apiParam {String} [port]
     * @apiParam {String} [username]
     * @apiParam {String} [password]
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     * @apiParamExample {json} Request-Example:
     * {
     *      "url": "192.168.2.105",
     *      "port": "1081",
     *      "username": "test",
     *      "password": "test"
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success"
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "status": 400,
     *     "errors": "Invalid params"
     * }
     *
     */
    async updateProxy(req, res, next) {
        const { id } = req.params;
        const proxy = req.body;

        if(!proxy.url || proxy.url.trim() === '') {
            return next(new Error("Invalid params"));
        }
        
        try {
            await ProxyModel
                .updateOne({ _id: id }, proxy)
                .exec();
        } catch (e) {
            console.log(e);
            return next(e);
        }

        return res.json({status: 'success'});
    }

    /**
     * @api {delete} /proxies/:id deleteProxy
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {String} id
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success"
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     */
    async deleteProxy(req, res, next) {
        const { id } = req.params;

        if(!id) {
            return next(new Error("Invalid params"));
        }
        
        try {
            await ProxyModel
                .deleteOne({ _id: id })
                .exec();
        } catch (e) {
            console.log(e);
            return next(e);
        }

        return res.json({status: 'success'});
    }

    /**
     * @api {put} /proxies/:id/switch switchProxy
     * @apiGroup Proxies
     * @apiVersion 1.0.0
     *
     * @apiParam {String} id
     *
     * @apiHeader {String} Content-Type=application/json
     * @apiHeader {String} Authorization Bearer JWT
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": "success"
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     *     "message": "Invalid token",
     *     "user": false
     * }
     *
     */
    async switchProxy(req, res, next) {
        const { id } = req.params;
        let proxy;
        try {
            proxy = await ProxyModel
                .findOne({ _id: id })
                .exec();
        } catch (e) {
            console.log(e);
            return next(e);
        }

        if(!proxy) {
            return next(new Error("Proxy not found", 404));
        }

        try {
            await ProxyModel
                .updateOne({ _id: id}, { active: !proxy.active})
                .exec();
        } catch (e) {
            console.log(e);
            return next(e);
        }

        return res.json({
            status: 'success',
            active: !proxy.active
        });
    }
}

export default new ProxyController();
