import { invokeMap } from 'lodash';
import DefaultController from './DefaultController';
import ImportRequestsController from './ImportRequestsController';
import AuthController from './AuthController';
import OffersController from './OffersController';
import ImportController from "./ExportController";
import HeadingController from "./HeadingController";
import CallcenterImportRequestsController from "./CallcenterImportRequestsController";
import CIRUrlController from "./CIRUrlController";
import ProxyController from "./ProxyController";
import ObyavaCategoriesController from "./obyava/ObyavaCategoriesController";

const controllers = {
    DefaultController,
    ImportRequestsController,
    AuthController,
    OffersController,
    ImportController,
    HeadingController,
    CallcenterImportRequestsController,
    CIRUrlController,
    ObyavaCategoriesController,
    ProxyController,
};

export function initControllers(router) {
    invokeMap(controllers, 'initRoutes', router);
}
