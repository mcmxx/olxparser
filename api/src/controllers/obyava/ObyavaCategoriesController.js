import Sequelize from "sequelize";
import Controller, { VERB } from "../../core/Controller";
import { OBYAVA_CATEGORIES_URL } from "../../constants/urls";
import { categoriesModel } from "../../models/obyava";
import Error from "../../core/Error";
import ParamsService from "../../services/obyava/ParamsService";

const Op = Sequelize.Op;

class ObyavaCategoriesController extends Controller {
    get routes() {
        return [
            {
                route: OBYAVA_CATEGORIES_URL,
                verb: VERB.GET,
                handler: this.getCategories
            },
            {
                route: `${OBYAVA_CATEGORIES_URL}/params/:categoryId`,
                verb: VERB.GET,
                handler: this.getParamsAndValues
            },
            {
                route: `${OBYAVA_CATEGORIES_URL}/values/:categoryId/:paramId`,
                verb: VERB.GET,
                handler: this.getValues
            },
            {
                route: `${OBYAVA_CATEGORIES_URL}/:id`,
                verb: VERB.GET,
                handler: this.getCategory
            }
        ];
    }

    async getValues(req, res, next) {
        const {
            categoryId,
            paramId
        } = req.params;

        let category;

        try {
            category = await categoriesModel.findByPk(categoryId);
        } catch (e) {
            console.log(e);
            return next(e);
        }

        if(!category) {
            return next(new Error("Category not found", 404));
        }

        const { is_new_type } = category;

        let values;
        try {
            values = await ParamsService.getValues(categoryId, paramId, is_new_type);
        } catch (e) {
            console.log(e);
            return next(e);
        }

        return res.json({
            status: 'success',
            values: is_new_type ? values[0] : values.values
        });
    }

    async getCategories(req, res, next) {
        const {
            query: {
                limit,
                offset,
                search,
                order,
                orderBy,
                parentId,
                isNewType
            }
        } = req;

        const whereQuery = {
            active: 1
        };
        if(search && search.trim() !== '') {
            whereQuery.name = {
                [Op.like]: `%${search.trim()}%`
            }
        } else {
            whereQuery.parent_id = +parentId || null;
        }

        if(isNewType) {
            whereQuery.is_new_type = 1;
        }

        const categories = await categoriesModel.findAll({
            where: whereQuery,
            limit: +limit || undefined,
            offset: +offset || 0,
            order: [
                [
                    orderBy || 'order',
                    order || 'asc'
                ]
            ],
            include: [
                {
                    model: categoriesModel,
                    as: 'children'
                }
            ]
        });

        return res.json({
           status: 'success',
           items: categories
        });
    }

    async getCategory(req, res, next) {
        const { id } = req.params;
        const category = await categoriesModel.findByPk(
            id,
            {
                include: [
                    {
                        model: categoriesModel,
                        as: 'children'
                    }
                ]
            }
        );
        if(!category) {
            return next(new Error("Not found", 404));
        }

        return res.json({
            status: 'success',
            items: category
        });
    }

    async getParamsAndValues(req, res, next) {
        const {
            categoryId
        } = req.params;

        const {
            paramId,
            valueId
        } = req.query;

        let category;

        try {
            category = await categoriesModel.findByPk(categoryId);
        } catch (e) {
            console.log(e);
            return next(new Error("Category not found", 404));
        }

        if(!category) {
            return next(new Error("Category not found", 404));
        }

        const params = await ParamsService.getAndNormalizeParams(
            categoryId,
            category.is_new_type,
            paramId,
            valueId
        );

        return res.json({
            status: 'success',
            category: params
        });
    }
}

export default new ObyavaCategoriesController();
