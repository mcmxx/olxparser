import mongoConnection from './mongodb';
import obyavaDbConnection from './obyavadb'

export {
    mongoConnection,
    obyavaDbConnection
};
