import { Sequelize } from "sequelize";
import config from '../config';

const {
    host,
    port,
    dbname,
    user,
    password,
    dialect
} = config.obyavadb;

const sequelize = new Sequelize(dbname, user, password, {
    host,
    username: user,
    port,
    dialect,
    password,
    database: dbname
});

sequelize.authenticate()
    .then(() => console.log("authenticated"))
    .catch((e) => {
        console.log(e);
        console.log("not authenticated");
    });

export default sequelize;
