import yml from 'yandex-market-language';
import {
    toString,
    truncate,
    toNumber
} from 'lodash';
import currencies,
{
    currencyAssoc,
    DEFAULT_CURRENCY_ID
} from "../constants/currencies";
import CategoryService, { CATEGORY_TYPE } from "../services/obyava/CategoryService";
import { paramsModel } from "../models/obyava";
import {HeadingModel} from "../models";
import {HeadingService} from "../services";

const headingDetails = {};

export const YMLConverter = async (importRequest, offersList) => {

    const convertData = await convertOffers(offersList);

    const YMLJson = {
        name: importRequest.email,
        company: importRequest.phone,
        url: importRequest.olxAccountUrl,
        currencies,
        'delivery-options': [],
        categories: convertData.categories,
        offers: convertData.offers
    };

    return yml(YMLJson).end({ pretty: true });
};

const convertOffers = async (offerList) => {
    const categoriesList = [];
    const offersList = [];

    for (let offerIndex = 0; offerIndex < offerList.length; offerIndex++) {
        const offer = offerList[offerIndex];
        const category = await HeadingService.getCategoryByOffer(offer);
        if(category) {
            if(categoriesList.find(item => item.id === toString(category.id))) {
                categoriesList.push({
                    id: toString(category.id),
                    name: category.name
                });
            }
            offersList.push(await convertOffer(offer, category.id, offer.headingString));
        }
    }

    return {
        offers: offersList,
        categories: categoriesList
    };
};

const getDetailsMap = async (headingString, categoryId) => {
    const heading = await HeadingModel.findOne({heading: headingString}).exec();
    if(!heading || !heading.category) return null;
    if(heading.category.id && heading.category.id === categoryId) {
        return heading.parameters || null;
    }
    if(!heading.parameters) return null;

    const categorizedParameter = heading.parameters.find(param => param.category);
    if(!categorizedParameter || !categorizedParameter.values) return null;
    const categorizedValue = categorizedParameter.values.find(val => val.categoryId === categoryId);
    return categorizedValue && categorizedValue.parameters ? categorizedValue.parameters : null;
}

const detailsConvertor = async (details, headingString, categoryId) => {
    const detailsMap = getDetailsMap(headingString, categoryId);
    const detailsResult = [];
    for (let i = 0; i < details.length; i++) {
        const {
            measure,
            value
        } = details[i];
        if(measure.replace(new RegExp("\\r?\\n?\\t?", "g"), "")
            .trim().length === 0) continue;
        for(let j = 0; j < value.length; j++) {
            const parameter = detailsMap
                .find(param => param.active && param.name.toLowerCase() === measure.toLowerCase());
            if (parameter) {
                const parameterName = parameter.obyavaParameterName || parameter.name;
                let newValue = value[j].replace(new RegExp("\\r?\\n?\\t?", "g"), "");
                if (parameter.values && Array.isArray(parameter.values)) {
                    const obyavaValue = parameter.values.find(val => val.name.toLowerCase() === value[j].toLowerCase());
                    if (obyavaValue) newValue = obyavaValue.obyavaValueName || value[j];
                }
                detailsResult.push({
                    name: parameterName,
                    value: newValue
                });
            } else {
                detailsResult.push({
                    name: measure,
                    value: value[j]
                })
            }
        }
    }
    return detailsResult;
};


const currencyFounder = (currency) => currencyAssoc.reduce(
        (acc ,item) => {
            if(item.names.includes(toString(currency).toLowerCase())) {
                acc = item.id;
            }
            return acc;
        },
        DEFAULT_CURRENCY_ID
    );


const convertOffer = async (offer, categoryId, headingString) => {


    return {
        id: truncate(offer.offerId, {length: 20, omission: ''}),
        available: true,
        url: offer.url,
        price: offer.price ? toNumber(offer.price.amount.replace(/\s/g, "")) : 0,
        currencyId: currencyFounder(offer.price ? offer.price.volume : DEFAULT_CURRENCY_ID),
        categoryId: toString(categoryId),
        name: truncate(offer.title, {length: 120}),
        description: offer.description,
        param: await detailsConvertor(offer.details, headingString, categoryId),
        picture: offer.images.filter((item, index) => index < 10)
    }
};
