import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";
import mongoPaginate from "mongoose-paginate-v2";
import { mongoConnection } from "../db";

const Schema = mongoose.Schema;
/*
parameters array of parameter
parameter : {olxparamterId, obyavaId, olxName, obyavaName, values}
values array of value
value : {
    olxValue,
    obyavaValueId,
    obyavaName
}
 */

const HeadingSchema = new Schema({
    heading: {
        type: String,
        index: true,
        unique: true
    },
    category: {
        type: {
            id: {
                type: Number,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            path: {
                type: String,
                required: true
            },
            is_new_type: {
                type: Boolean,
                default: false
            }
        },
        default: null
    },
    parameters: {
        type: Array,
        default: null
    },
    createdAt: Date
});

autoIncrement.initialize(mongoConnection);

HeadingSchema.plugin(autoIncrement.plugin, 'Heading');
HeadingSchema.plugin(mongoPaginate);

export default mongoConnection.model('Heading', HeadingSchema);
