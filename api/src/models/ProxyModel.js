import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import { mongoConnection } from '../db';

const Schema = mongoose.Schema;

const ProxySchema = new Schema({
    url: String,
    port: {
        type: String,
        default: '1080'
    },
    username: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        default: ''
    },
    active: {
        type: Boolean,
        default: true
    },
    type: {
        type: String,
        default: "SOCKS5"
    }
});

ProxySchema.plugin(mongoosePaginate);

export default mongoConnection.model('Proxy', ProxySchema);
