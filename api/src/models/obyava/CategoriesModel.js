export default function (sequelize, DataTypes) {
    return sequelize.define(
        'categories',
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            parent_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categories',
                    key: 'id'
                }
            },
            category_main_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categories',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            alias: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            meta_description: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            icon: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            location: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            currency: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            price_rounded: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            price_hidden: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            has_delivery: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 1
            },
            operation: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            is_dating: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            active: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            is_new_type: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            show_children: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            order: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            user_classified_limit: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: true,
            },
            service_activated_at: {
                type: DataTypes.DATE,
                allowNull: true
            },
            classified_count: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            show_all_parameter: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            all_parameter_name: {
                type: DataTypes.STRING(300),
                allowNull: true
            },
            all_parameter_position: {
                type: DataTypes.ENUM('first','last'),
                allowNull: true,
                defaultValue: 'first'
            },
            all_parameter_icon: {
                type: DataTypes.STRING(500),
                allowNull: true
            },
            next_classified_ua: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            next_classified_ru: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            tags: {
                type: DataTypes.STRING(5000),
                allowNull: true
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true
            }
        },
        {
            tableName: 'categories',
            timestamps: false
        }
    );
};
