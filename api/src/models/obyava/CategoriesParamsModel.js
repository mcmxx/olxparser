export default function (sequelize, DataTypes) {
    return sequelize.define(
        'categoriesParams',
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            parent_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categoriesParams',
                    key: 'id'
                }
            },
            param_group_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categoriesParamsGroups',
                    key: 'id'
                }
            },
            param_value_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categoriesParamsValues',
                    key: 'id'
                }
            },
            category_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categories',
                    key: 'id'
                }
            },
            type: {
                type: DataTypes.ENUM('I','S','SC','C','R'),
                allowNull: false
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            unit: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            main: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            operation: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            view_list: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            preset: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            multiple: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            hidden: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            hidden_filter: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            create_classified_show: {
                type: DataTypes.INTEGER(3).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            rules: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            order: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            created_at: {
                type: DataTypes.DATE
            },
            updated_at: {
                type: DataTypes.DATE
            }
        },
        {
            tableName: 'categories_params',
            timestamps: false
        }
    );
};
