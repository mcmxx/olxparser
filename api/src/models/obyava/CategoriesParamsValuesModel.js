export default function (sequelize, DataTypes) {
    return sequelize.define(
        'categoriesParamsValues',
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            param_id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'categoriesParams',
                    key: 'id'
                }
            },
            param_next_id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categoriesParams',
                    key: 'id'
                }
            },
            param_parent_id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categoriesParams',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            icon: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            order: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            created_at: {
                type: DataTypes.DATE
            },
            updated_at: {
                type: DataTypes.DATE
            }
        },
        {
            tableName: 'categories_params_values',
            timestamps: false
        }
    )
}
