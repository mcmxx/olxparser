export default function (sequelize, DataTypes) {
    return sequelize.define(
        'categoryParam',
        {
            categoryId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'categories',
                    key: 'id'
                },
                field: 'category_id'
            },
            paramId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: true,
                references: {
                    model: 'params',
                    key: 'id'
                },
                field: 'param_id'
            },
            order: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            }
        },
        {
            tableName: 'category_param',
            timestamps: false
        }
    );
};
