export default function (sequelize, DataTypes) {
    return sequelize.define(
        'categoryParamValue',
        {
            categoryId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                references: {
                    model: 'categories',
                    key: 'id'
                },
                field: 'category_id'
            },
            paramValueId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                references: {
                    model: 'paramValues',
                    key: 'id'
                },
                field: 'param_value_id'
            }
        },
        {
            tableName: 'category_param_value',
            timestamps: false
        }
    )
}
