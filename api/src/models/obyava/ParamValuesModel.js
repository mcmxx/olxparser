export default function (sequelize, DataTypes) {
    return sequelize.define(
        'paramValues',
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            param_id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            name_ua: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true
            }
        },
        {
            tableName: 'param_values',
            timestamps: false
        }
    );
};
