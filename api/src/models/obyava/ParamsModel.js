export default function (sequelize, DataTypes) {
    return sequelize.define(
        'params',
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            type: {
                type: DataTypes.ENUM('I','S','SC','C','R'),
                allowNull: false
            },
            param_group_id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: true,
                references: {
                    model: 'paramGroups',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            name_ua: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            unit: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            multiple: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            view_list: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            hidden: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            hidden_filter: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            create_classified_show: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: 0
            },
            rules: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true
            }
        },
        {
            tableName: 'params',
            timestamps: false
        }
    );
};
