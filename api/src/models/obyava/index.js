import { DataTypes } from "sequelize";
import sequelize from "../../db/obyavadb";
import CategoriesModel from "./CategoriesModel";
import CategoriesParamsModel from "./CategoriesParamsModel";
import CategoriesParamsValuesModel from "./CategoriesParamsValuesModel";
import CategoryParamModel from "./CategoryParamModel";
import ParamsModel from "./ParamsModel";
import ParamValuesModel from "./ParamValuesModel";
import CategoryParamValueModel from "./CategoryParamValueModel";


export const CATEGORY_TYPE = {
    OLD: 0,
    NEW: 1
};

const categoriesModel = CategoriesModel(sequelize, DataTypes);
const categoriesParamsModel = CategoriesParamsModel(sequelize, DataTypes);
const categoriesParamsValuesModel = CategoriesParamsValuesModel(sequelize, DataTypes);
const categoryParamModel = CategoryParamModel(sequelize, DataTypes);
const paramsModel = ParamsModel(sequelize, DataTypes);
const paramValuesModel = ParamValuesModel(sequelize, DataTypes);
const categoryParamValueModel = CategoryParamValueModel(sequelize, DataTypes);

categoriesModel.hasMany(categoriesModel, { foreignKey: 'parent_id', as: 'children' });
categoriesModel.belongsTo(categoriesModel, { foreignKey: 'parent_id', as: 'parent' });

//Old-style params
categoriesModel.hasMany(categoriesParamsModel, { foreignKey: 'category_id', as: 'params' });
categoriesParamsModel.belongsTo(categoriesModel, { foreignKey: 'category_id', as: 'category' });

categoriesParamsModel.hasMany(categoriesParamsModel, { foreignKey: 'parent_id', as: 'children' });
categoriesParamsModel.belongsTo(categoriesParamsModel, { foreignKey: 'parent_id', as: 'parent' });

categoriesParamsModel.hasMany(categoriesParamsValuesModel, {
   sourceKey: 'id',
   foreignKey: 'param_id',
   as: 'paramValues'
});

categoriesParamsValuesModel.belongsTo(categoriesParamsModel, {
    sourceKey: 'param_id',
    foreignKey: 'id',
    as: 'valueParam'
});

//New-style params
categoriesModel.belongsToMany(paramsModel, {
    through: categoryParamModel,
    as: 'parameters'
});
paramsModel.belongsToMany(categoriesModel, {
    through: categoryParamModel,
    as: 'parameters'
});

paramsModel.hasMany(paramValuesModel, {
    sourceKey: 'id',
    foreignKey: 'param_id',
    as: 'values'
});

paramValuesModel.belongsToMany(categoriesModel, {
    through: categoryParamValueModel,
    as: 'newValues'
});

categoriesModel.belongsToMany(paramValuesModel, {
    through: categoryParamValueModel,
    as: 'category'
});

export {
    categoriesModel,
    categoriesParamsModel,
    categoriesParamsValuesModel,
    paramsModel,
    categoryParamModel,
    paramValuesModel,
    categoryParamValueModel
}
