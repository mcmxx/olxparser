import { merge } from "lodash";
import {
    HeadingModel,
    OffersModel
} from "../models";
import {
    categoriesModel as obyavaCategoriesModel,
    paramsModel as obyavaNewParamsModel,
    categoriesParamsModel as obyavaOldParamsModel,
    paramValuesModel as obyavaNewValuesModel,
    categoriesParamsValuesModel as obyavaOldValuesModel, categoriesModel
} from "../models/obyava";
import { CategoryService as ObyavaCategoryService } from "./obyava";

class HeadingService {
    async isExist(headingName, id = null) {
        let heading;
        if(id) {
            heading = await HeadingModel
                .findOne({
                    heading: headingName,
                    _id: {
                        $ne: id
                    }
                })
                .exec();
        } else {
            heading = await HeadingModel
                .findOne({heading: headingName})
                .exec();
        }
        return !!heading;
    }

    async hasOffers(headingId) {
        const offers = await OffersModel
            .find({
                headingId
            })
            .exec();
        let offersIds = null;
        if(offers && offers.docs) {
            offersIds = offers.docs.map(offer => offer._id);
        }

        return offersIds;
    }

    getCity(heading) {
        const re = /^\S+\s+([\S\s]+)/g;
        const matches = re.exec(heading);
        if(matches && matches.length > 1) {
            return matches[1];
        } else {
            return null;
        }
    }

    makeStringFromArrayHeading(heading) {
        if(!Array.isArray(heading) || heading.length === 0) {
            return null;
        }

        const city = this.getCity(heading[0]);
        const convertedHeading = city
            ? heading.map(item => item.replace(city, "").trim())
            : heading;
        convertedHeading.shift();
        const slashRegexp = /((\s|\t|\r|\n)*\/(\s|\t|\r|\n)*)+/g;
        return convertedHeading.join("/").replace(slashRegexp, "/");
    }

    async addHeadingOrGetId(heading) {
        const headingString = this.makeStringFromArrayHeading(heading);
        const headingFromDb = await HeadingModel
            .findOne({heading: headingString})
            .exec();

        if(headingFromDb) {
            return {
                id: headingFromDb._id,
                headingString: headingString
            }
        } else {
            const headingModel = new HeadingModel({ heading: headingString, createdAt: new Date() });
            await headingModel.save();
            const headingFromDb = await HeadingModel
                .findOne({heading: headingString})
                .exec();
            if(headingFromDb) {
                return {
                    id: headingFromDb._id,
                    headingString: headingString
                }
            }
        }

        return null;
    }

    async setAssocWithCategory(headingId, categoryId) {
        const heading = await HeadingModel.findOne({ _id: headingId }).exec();
        if(!heading) {
            throw new Error('Heading not found');
        }


        const category = await obyavaCategoriesModel.findByPk(categoryId);
        if(!category) {
            throw new Error('Category not found');
        }

        this.resetAllParameters(heading);
        heading.category = {
            id: category.id,
            name: category.name,
            path: await ObyavaCategoryService.makePath(categoryId),
            is_new_type: category.is_new_type
        };

        await HeadingModel.updateOne({ _id: heading }, heading);
    }

    async setValueAsCategoryAssoc(params) {
        const {
            headingId,
            categoryId,
            parameterName,
            valueName
        } = params;

        const heading = await HeadingModel.findOne({_id: headingId}).exec();
        if(!heading) throw new Error("Heading not found");

        const category = await obyavaCategoriesModel.findByPk(categoryId);
        if(!category) throw new Error("Category not found");

        if(!heading.parameters || !Array.isArray(heading.parameters)) {
            throw new Error("No parameters set");
        }
        const parameterIndex = heading.parameters.findIndex(parameter => parameter.name === parameterName);
        if(parameterIndex === -1) {
            throw new Error("Parameter not found");
        }
        if(!heading.parameters[parameterIndex].values || !Array.isArray(heading.parameters[parameterIndex].values)) {
            throw new Error("No values set");
        }
        if(!heading.parameters[parameterIndex].category) {
            throw new Error("Parameters is not configured to map category");
        }

        const offers = await OffersModel.find({headingId}).exec();
        const filteredOffers = offers && Array.isArray(offers)
            ? offers.filter(offer => {
                for(const detail of offer.details) {
                    console.log("detail", detail);
                    if(detail.measure.toLowerCase() !== parameterName.toLowerCase()) continue;
                    return (Array.isArray(detail.value)
                        && detail.value.findIndex(value => value.toLowerCase() === valueName.toLowerCase()) > -1) ||
                        (!Array.isArray(detail.value) && detail.value.toLowerCase() === valueName.toLowerCase());
                }
            })
            : null;

        const categoryPath = await ObyavaCategoryService.makePath(categoryId);

        const newValues = heading.parameters[parameterIndex].values
            .map((value) => {
                if(value.name.toLowerCase() === valueName.toLowerCase()) {
                    return {
                        name: value.name,
                        categoryId,
                        categoryPath,
                        is_new_type: category.is_new_type,
                        parameters: heading.parameters
                            .filter(param => param.name !== parameterName)
                            .map(param => {
                                let values = [];
                                if(filteredOffers) {
                                    const valuesSet = new Set();
                                    filteredOffers.forEach(offer => {
                                        offer.details.forEach(detail => {
                                            if(detail.measure.toLowerCase() === param.name.toLowerCase()) {
                                                for(const value of detail.value) {
                                                    valuesSet.add(
                                                        value.replace(
                                                            new RegExp("\\r?\\n?\\t?", "g"),
                                                            "")
                                                    );
                                                }
                                            }
                                        })
                                    })
                                    valuesSet.forEach(value => values.push({name: value}));
                                } else {
                                    values = param.values.map(val => ({name: val.name}));
                                }
                                param.values = values;
                                return param;
                        })
                    }
                }
                return value;
            });
        heading.parameters[parameterIndex].values = newValues;

        await HeadingModel.updateOne({_id: headingId}, heading).exec();
    }

    async setValueAssoc(params) {
        const {
            headingId,
            categoryId,
            parameterName,
            valueName,
            valueId,
            isSubParameter
        } = params;

        const heading = await HeadingModel.findOne({_id: headingId}).exec();
        if(!heading) throw new Error("Heading not found");

        const category = await obyavaCategoriesModel.findByPk(categoryId);
        if(!category) throw new Error("Category not found");

        const value = category.is_new_type
            ? await obyavaNewValuesModel.findByPk(valueId)
            : await obyavaOldValuesModel.findByPk(valueId);

        if(!value) throw new Error("Value not found");
        const newValue = {
            obyavaValueId: valueId,
            obyavaValueName: value.name
        };

        if (!heading.parameters || !Array.isArray(heading.parameters)) {
            throw new Error("No parameters set");
        }

        let saveFlag = false;
        if(!isSubParameter) {
            const parameterIndex = heading.parameters.findIndex(parameter => parameter.name === parameterName);
            if (parameterIndex === -1) {
                throw new Error("Parameter not found");
            }
            if (heading.parameters[parameterIndex].values && Array.isArray(heading.parameters[parameterIndex].values)) {
                const valueIndex = heading.parameters[parameterIndex].values.findIndex(value => value.name === valueName);
                if (valueIndex > -1) {
                    heading.parameters[parameterIndex].values[valueIndex] =
                        merge(heading.parameters[parameterIndex].values[valueIndex], newValue);
                } else {
                    newValue.name = valueName;
                    heading.parameters[parameterIndex].values.push(newValue);
                }
                saveFlag = !saveFlag;
            } else if (!heading.parameters[parameterIndex].values) {
                newValue.name = valueName;
                heading.parameters[parameterIndex].values = [newValue];
                saveFlag = !saveFlag;
            }
        } else {
            const parameterIndex = heading.parameters.findIndex(parameter => parameter.category);
            if (parameterIndex === -1) {
                throw new Error("Parameter not found");
            }
            const valueIndex = heading.parameters[parameterIndex].values
                .findIndex(value => value.categoryId === categoryId);
            if(valueIndex === -1) {
                throw new Error("Value with category");
            }
            const valueParameterIndex = heading.parameters[parameterIndex].values[valueIndex].parameters
                .findIndex(parameter => parameter.name === parameterName);
            if(valueParameterIndex === -1) {
                throw new Error("Value-parameter not found");
            }
            const valueParameter = heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex];
            if(valueParameter.values && Array.isArray(valueParameter.values)) {
                const index = valueParameter.values.findIndex(value => value.name === valueName);
                if(index > -1) {
                    valueParameter.values[index] = merge(valueParameter.values[index], newValue);
                }  else {
                    newValue.name = valueName;
                    valueParameter.values.push(newValue);
                }
                saveFlag = !saveFlag;
            } else {
                newValue.name = valueName;
                valueParameter.values = [newValue];
                saveFlag = !saveFlag;
            }
            heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex] = valueParameter;
        }

        if(saveFlag) await HeadingModel.updateOne({_id: headingId}, heading).exec();
    }

    async setParameterAssoc(params) {
        const {
            headingId,
            categoryId,
            parameterName,
            obyavaParameterId,
            isSubParameter
        } = params;

        console.log(params);

        const heading = await HeadingModel.findOne({_id: headingId}).exec();
        if(!heading) throw new Error("Heading not found");

        const category = await obyavaCategoriesModel.findByPk(categoryId);
        if(!category) throw new Error("Category not found");

        const parameter = category.is_new_type
            ? await obyavaNewParamsModel.findByPk(obyavaParameterId)
            : await obyavaOldParamsModel.findByPk(obyavaParameterId);

        if(!parameter) throw new Error("Parameter not found");
        const newParameter = {
            obyavaParameterId,
            obyavaParameterName: parameter.name
        };

        let saveFlag = false;
        if(!isSubParameter) {
            if (heading.parameters && Array.isArray(heading.parameters)) {
                const parameterIndex = heading.parameters.findIndex(parameter => parameter.name === parameterName);
                if (parameterIndex > -1) {
                    heading.parameters[parameterIndex] = merge(heading.parameters[parameterIndex], newParameter);
                    this.resetParametersValues(heading.parameters[parameterIndex]);
                } else {
                    newParameter.name = parameterName;
                    heading.parameters.push(newParameter);
                }
                saveFlag = !saveFlag;
            } else if (!heading.parameters) {
                newParameter.name = parameterName;
                heading.parameters = [newParameter];
                saveFlag = !saveFlag;
            }
        } else {
            if(heading.parameters && Array.isArray(heading.parameters)) {
                const parameterIndex = heading.parameters.findIndex(parameter => parameter.category);
                if(parameterIndex > -1) {
                    const valueIndex = heading.parameters[parameterIndex].values
                        .findIndex(value => value.categoryId === categoryId);
                    if(valueIndex > -1) {
                        const valueParameterIndex = heading.parameters[parameterIndex].values[valueIndex].parameters
                            .findIndex(parameter => parameter.name === parameterName);
                        if(valueParameterIndex > -1) {
                            heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex]
                                = merge(heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex], newParameter);
                            heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex].values =
                                heading.parameters[parameterIndex].values[valueIndex].parameters[valueParameterIndex].values
                                    .map(value => ({name: value.name}));
                        } else {
                            newParameter.name = parameterName;
                            heading.parameters[parameterIndex].values[valueIndex].parameters.push(newParameter);
                        }
                        saveFlag = !saveFlag;
                    }
                }
            }
        }

        if (saveFlag) await HeadingModel.updateOne({_id: headingId}, heading).exec();
    }

    resetParametersValues(parameter) {
        if(parameter.values && Array.isArray(parameter.values)) {
            parameter.values.forEach(value => {
                delete value.obyavaValueId;
                delete value.obyavaValueName
            })
        }
    };

    resetAllParameters(heading) {
        if(heading.parameters && Array.isArray(heading.parameters)) {
            heading.parameters.forEach(parameter => {
                delete parameter.obyavaParameterId;
                delete parameter.obyavaParameterName;
                this.resetParametersValues(parameter);
            })
        }
    }

    async resetAssocWithCategory(headingId) {
        const heading = await HeadingModel.findOne({ _id: headingId }).exec();
        if(!heading) {
            throw new Error('Heading not found');
        }

        await HeadingModel.updateOne({ _id: headingId }, { category: undefined });
    }

    async getCategoryByHeading(heading) {
        const { id } = await this.addHeadingOrGetId(heading);
        const res = await HeadingModel.findOne({ _id: id }).exec();
        if(!res) {
            throw new Error('Heading not found');
        }

        return res.category;
    }

    async getCategoryPathByHeadingId(headingId) {
        const heading = await HeadingModel.findOne({_id: headingId}).exec();
        if(heading && heading.category && heading.category.path) {
            return heading.category.path;
        } else {
            return '';
        }
    }

    async getCategoryByOffer(offer) {
        const headingString = offer.headingString;
        if(!headingString) return offer.categoryId;

        const heading = await HeadingModel.findOne({heading: headingString}).exec();
        if(!heading) return offer.categoryId;

        function returnByDefault() {
            const categoryId = heading.category && heading.category.id
                ? heading.category.id
                : offer.categoryId;
            if(categoryId) {
                const category = categoriesModel.findByPk(categoryId);
                return category ? category : null;
            }

            return null;
        }


        if(!heading.parameters || !Array.isArray(heading.parameters)) {
            returnByDefault();
        }

        const categorizedParameter = heading.parameters.find(param => param.category);
        if(!categorizedParameter || !categorizedParameter.values) {
            returnByDefault();
        }

        if(!offer.details || !Array.isArray(offer.details)) {
            returnByDefault();
        }

        const offerDetailIndex =
            offer.details
                .findIndex(detail => detail.measure.toLowerCase() === categorizedParameter.name.toLowerCase());
        if(!offerDetailIndex) {
            returnByDefault();
        }

        const offerDetailsValues = offer.details[offerDetailIndex].value;
        const value = categorizedParameter.values.find(item => {
            if(Array.isArray(offerDetailsValues)) {
                const index = offerDetailsValues
                    .findIndex(offerValue => offerValue.replace(new RegExp("\\r?\\n?\\t?", "g"), "") === item.name);
                return index > -1;
            } else {
                return offerDetailsValues.replace(new RegExp("\\r?\\n?\\t?", "g"), "") === item.name;
            }
        });

        if(!value || !value.categoryId) {
            returnByDefault();
        }

        const category = await categoriesModel.findByPk(value.categoryId);
        if(!category) {
            returnByDefault();
        }

        return category;
    }

    async toggleParameter(headingId, parameterName) {
        const heading = await HeadingModel.findOne({ _id: headingId }).exec();
        if(!heading) {
            throw new Error('Heading not found');
        }

        if(heading.parameters && Array.isArray(heading.parameters)) {
            const index =  heading.parameters.findIndex(parameter => parameter.name === parameterName);
            if(index > -1) {
                heading.parameters[index].active = !heading.parameters[index].active;
                await HeadingModel.updateOne({_id: headingId}, heading).exec();
            }
        }
    }

    async toggleParameterAsCategory(headingId, parameterName) {
        const heading = await HeadingModel.findOne({ _id: headingId }).exec();
        if(!heading) {
            throw new Error('Heading not found');
        }

        if(heading.parameters && Array.isArray(heading.parameters)) {
            const index =  heading.parameters.findIndex(parameter => parameter.name === parameterName);
            if(index > -1) {
                heading.parameters[index].category = heading.parameters[index].category
                    ? !heading.parameters[index].category
                    : true;

                if(heading.parameters[index].category) {
                    heading.parameters = heading.parameters.map((parameter, i) => {
                        if(i !== index) parameter.category = false;
                        return parameter;
                    })
                }

                await HeadingModel.updateOne({_id: headingId}, heading).exec();
            }
        }
    }
}

export default new HeadingService();
