import { toInteger } from "lodash";
import {
    OffersModel,
    HeadingModel
} from "../models";
import OfferService from "./OfferService";

const MAX_OFFERS_CHUNK_SIZE = 500;

class OfferDetailService {
    async makeOfferDetailCollection() {
        const offersNumber = await OfferService.getAllOffersNumber();
        let pages = toInteger(offersNumber / MAX_OFFERS_CHUNK_SIZE);
        if(offersNumber % MAX_OFFERS_CHUNK_SIZE) pages++;
        for(let i = 0; i < pages; i++) {
            console.log("makeOfferDetailCollection page: " + i + " of " + pages);
            const offersList = await OffersModel
                .paginate(
                    null,
                    {
                        limit: MAX_OFFERS_CHUNK_SIZE,
                        offset: MAX_OFFERS_CHUNK_SIZE * i,
                        sort: [
                            ["createdAt", "desc"]
                        ]
                    });
            for(let j = 0; j < offersList.docs.length; j++) {
                const offer = offersList.docs[j];
                const headingString = offer.headingString;
                const details = offer.details;
                const heading = await HeadingModel
                    .findOne({ heading: headingString })
                    .exec();
                if(heading) {
                    const parameters = heading.parameters;
                    for(let k = 0; k < details.length; k++) {
                        const detailName = details[k].measure
                            .replace(new RegExp("\\r?\\n?\\t?", "g"), "")
                            .trim();
                        if(detailName.length === 0) continue;
                        const detailValues = details[k].value;
                        const index = parameters.findIndex(parameter =>
                            parameter.name.toLowerCase() === detailName.toLowerCase()
                        );
                        const newValues = Array.isArray(detailValues)
                            ? detailValues.map(value => (
                                {
                                    name: value.toLowerCase()
                                        .replace(new RegExp("\\r?\\n?\\t?", "g"), "")
                                        .trim()
                                }))
                            : [{name: detailValues.toLowerCase()
                                    .replace(new RegExp("\\r?\\n?\\t?", "g"), "")
                                    .trim()
                        }];
                        if(index === -1) {
                            parameters.push({
                                name: detailName.toLowerCase(),
                                active: true,
                                category: false,
                                values: newValues
                            });
                        } else {
                            if(!parameters[index].values) {
                                parameters[index].values = newValues
                            } else {
                                const valuesSet = [];
                                for(let valueIndex = 0; valueIndex < parameters[index].values.length; valueIndex++) {
                                    if(!valuesSet.find(value => value.name === parameters[index].values[valueIndex].name)) {
                                        valuesSet.push(parameters[index].values[valueIndex]);
                                    }
                                }
                                for(let newValueIndex = 0; newValueIndex < newValues.length; newValueIndex++) {
                                    if(!valuesSet.find(value => value.name === newValues[newValueIndex].name)) {
                                        valuesSet.push(newValues[newValueIndex]);
                                    }
                                }
                                parameters[index].values = valuesSet;
                            }
                        }
                    }
                    try {
                        await HeadingModel
                            .updateOne({ _id: heading._id }, { parameters })
                            .exec();
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }
    }
}

export default new OfferDetailService();
