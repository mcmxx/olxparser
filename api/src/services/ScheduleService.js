import schedule from "node-schedule";

class ScheduleService {
    addJob(jobName, where, handler) {
        return schedule.scheduleJob(where, async () => {
            console.log(`Schedule Job: ${jobName}`);
            await handler();
        });
    }

    cancelJob(job) {
        job.cancel();
    }
}

export default new ScheduleService();
