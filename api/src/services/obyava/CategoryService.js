import { isNil } from "lodash";
import { categoriesModel } from "../../models/obyava";
import HeadingService from "../HeadingService";

export const CATEGORY_TYPE = {
    OLX: 'OLX',
    OBYAVA: 'OBYAVA',
    CONVERTED: 'CONVERTED'
};

class CategoryService {
    async makePath(categoryId) {
        async function _getWithRecurse(id, path = []) {
            const category = await categoriesModel.findByPk(id);
            if(!category) {
                throw new Error("Category not found");
            }

            path.push(category.name);
            if(!isNil(category.parent_id)) {
                path = await _getWithRecurse(category.parent_id, path);
            }

            return path;
        }

        const path = await _getWithRecurse(categoryId);
        return path.reverse().join('/');
    }

    async getCategory(offer) {
        let type = CATEGORY_TYPE.OLX;
        const category = await HeadingService.getCategoryPathByHeadingId(offer.headingId);
        if(category) {
            type = CATEGORY_TYPE.CONVERTED;
        }
        return {
            category,
            type
        }
    }
}

export default new CategoryService();
