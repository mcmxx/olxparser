import {
    omit,
    toInteger } from 'lodash';
import {
    CATEGORY_TYPE,
    paramsModel,
    categoriesParamsModel,
    categoriesModel,
    categoriesParamsValuesModel,
    paramValuesModel,
    categoryParamValueModel
} from "../../models/obyava";
import sequelize from "../../db/obyavadb";
import {QueryTypes} from "sequelize";

class ParamsService {

    async getValues(categoryId, paramId, isNew = CATEGORY_TYPE.NEW) {
        if(isNew === CATEGORY_TYPE.NEW) {
            const param = await sequelize.query(`SELECT 
                param_values.id as id,
                param_values.param_id as param_id,
                param_values.name as name
                FROM param_values
                inner join params ON param_values.param_id = params.id AND params.id = ${paramId}
                inner join category_param_value ON category_param_value.category_id = ${categoryId} AND
                    category_param_value.param_value_id = param_values.id`,
                {
                    plain: true,
                    raw: true,
                    type: QueryTypes.RAW
                }
            );
            return param;
        } else {
            const param = await categoriesParamsModel.findByPk(
                paramId,
                {
                    include: {
                        model: categoriesParamsValuesModel,
                        as: "paramValues"
                    }
                });
            const result = omit(param.dataValues, ['paramValues']);
            result.values = param.paramValues && Array.isArray(param.paramValues)
                ? param.paramValues.map(value => value)
                : [];
            return result;
        }
    }

    async getAndNormalizeParams(categoryId, isNew = CATEGORY_TYPE.NEW, paramId = null, valueId = null) {
        const queriedParams = await this.getParams(categoryId, isNew, paramId, valueId);
        if (isNew === CATEGORY_TYPE.NEW) return queriedParams;
        if(!queriedParams || !queriedParams.dataValues) return [];
        const result = omit(queriedParams.dataValues, ['params']);
        result.parameters = queriedParams.params.map(parameter => {
            const localParameter = omit(parameter.dataValues, ['paramValues']);
            localParameter.values = parameter.paramValues && Array.isArray(parameter.paramValues)
                ? parameter.paramValues.map(value => value)
                : [];
            return localParameter;
        });
        return result;
    }

    async getParams(categoryId, isNew = CATEGORY_TYPE.NEW, paramId = null, valueId = null) {
        return isNew === CATEGORY_TYPE.NEW
            ? await this._getNewParams(categoryId)
            : this._getOldParams(categoryId, paramId, valueId);
    }

    async _getOldParams(categoryId, paramId = null, valueId = null) {
        const parent_id = toInteger(paramId) || null;
        const param_value_id = toInteger(valueId) || null;
        return categoriesModel.findByPk(
            categoryId,
            {
                include: [
                    {
                        model: categoriesParamsModel,
                        as: "params",
                        where: {
                            parent_id,
                            param_value_id
                        },
                        include: [
                            {
                                model: categoriesParamsValuesModel,
                                as: "paramValues"
                            }
                        ]
                    }
                ],
                plain: true
            });
    }

    async _getNewParams(categoryId) {
        return categoriesModel.findByPk(
            categoryId,
            {
                include: [
                    {
                        model: paramsModel,
                        as: "parameters",
                        include: [
                            {
                                model: paramValuesModel,
                                as: "values"
                            }
                        ]
                    }
                ]
            });
    }
}

export default new ParamsService();
