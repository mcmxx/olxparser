import { merge } from 'lodash';
import {
    GET_HEADINGS,
    GET_HEADING,
    CREATE_HEADING,
    UPDATE_HEADING,
    DELETE_HEADING,
    RESET_HEADING_FORM,
    SET_HEADING_CATEGORY_ASSOCIATION,
    RESET_HEADING_CATEGORY_ASSOCIATION,
    SET_HEADING_PARAMETER_ASSOCIATION,
    SET_HEADING_VALUE_ASSOCIATION,
    LOADED,
    LOADING
} from "../constants/actions";
import { HEADINGS_URL } from "../constants/urls";
import config from "../config";
import rest from "../utils/rest";
import { METHODS } from "../constants/methods";

const headingsUrl = `${config.backendUrl}${HEADINGS_URL}`;

const defaultQuery = {
    offset: 0,
    limit: 10,
    search: ''
};

const getHeadings = (incomingQuery = {}) => async dispatch => {
    dispatch({ type: LOADING, payload: {} });
    const queryData = merge(defaultQuery, incomingQuery);
    const responseData = await rest(headingsUrl, METHODS.GET, queryData);
    dispatch({ type: GET_HEADINGS, payload: responseData });
    dispatch({ type: LOADED, payload: {} });
};


const getHeading = id => async dispatch => {
    const responseData = await rest(`${headingsUrl}/${id}`, METHODS.GET);
    dispatch({ type: GET_HEADING, payload: responseData });
};


const createHeading = heading => async dispatch => {
    const responseData = await rest(headingsUrl, METHODS.POST, { heading });
    dispatch({ type: CREATE_HEADING, payload: responseData });
};

const updateHeading = (id, heading) => async dispatch => {
    const responseData = await rest(`${headingsUrl}/${id}`, METHODS.PUT, { heading });
    dispatch({ type: UPDATE_HEADING, payload: responseData });
};


const deleteHeading = id => async dispatch => {
    await rest(`${headingsUrl}/${id}`, METHODS.DELETE);
    dispatch({ type: DELETE_HEADING, payload: { id } });
};

const resetHeadingForm = () => async dispatch => {
    dispatch({ type: RESET_HEADING_FORM, payload: {} });
};

const setHeadingCategoryAssoc = (headingId, categoryId) => async dispatch => {
    const responseData = await rest(`${headingsUrl}/category`, METHODS.POST, { headingId, categoryId });
    dispatch({ type: SET_HEADING_CATEGORY_ASSOCIATION, payload: responseData });
}

const resetHeadingCategoryAssoc = id => async dispatch => {
    await rest(`${headingsUrl}/category/${id}`, METHODS.DELETE);
    dispatch({ type: RESET_HEADING_CATEGORY_ASSOCIATION, payload: {} });
}

const setHeadingParameterAssoc = (
    headingId,
    categoryId,
    parameterName,
    obyavaParameterId,
    isSubParameter = false
) => async dispatch => {
    const responseData = await rest(
        `${headingsUrl}/parameter`,
        METHODS.POST,
        {
            headingId,
            categoryId,
            parameterName,
            obyavaParameterId,
            isSubParameter
        });
    dispatch({ type: SET_HEADING_PARAMETER_ASSOCIATION, payload: responseData });
}

const setHeadingValueAssoc = (
        headingId,
        categoryId,
        parameterName,
        valueName,
        valueId,
        categoryFlag = false,
        isSubParameter = false
) => async dispatch => {
    const responseData = await rest(
        `${headingsUrl}/value`,
        METHODS.POST,
        {
            headingId,
            categoryId,
            parameterName,
            valueName,
            valueId,
            categoryFlag,
            isSubParameter
        }
    );
    dispatch({ type: SET_HEADING_VALUE_ASSOCIATION, payload: responseData });
}

export {
    getHeadings,
    getHeading,
    createHeading,
    updateHeading,
    deleteHeading,
    resetHeadingForm,
    setHeadingCategoryAssoc,
    resetHeadingCategoryAssoc,
    setHeadingParameterAssoc,
    setHeadingValueAssoc
}
