import { merge } from 'lodash';
import {
    GET_OBYAVA_CATEGORIES,
    GET_OBYAVA_CATEGORY,
    GET_OBYAVA_CATEGORY_PARAMS,
    LOADED,
    LOADING
} from "../../constants/actions";
import { OBYAVA_CATEGORIES_URL } from "../../constants/urls";
import config from "../../config";
import rest from "../../utils/rest";
import { METHODS } from "../../constants/methods";

const categoriesUrl = `${config.backendUrl}${OBYAVA_CATEGORIES_URL}`;

const defaultQuery = {
    offset: 0,
    limit: 10,
    search: ''
};

const getCategories = (incomingQuery = {}) => async dispatch => {
    dispatch({ type: LOADING, payload: {} });
    const queryData = merge(defaultQuery, incomingQuery);
    const responseData = await rest(categoriesUrl, METHODS.GET, queryData);
    dispatch({ type: GET_OBYAVA_CATEGORIES, payload: responseData });
    dispatch({ type: LOADED, payload: {} });
}

const getCategory = id => async dispatch => {
    const responseData = await rest(`${categoriesUrl}/${id}`, METHODS.GET);
    dispatch({ type: GET_OBYAVA_CATEGORY, payload: responseData });
}

const getParamsByCategory = categoryId => async dispatch => {
    const responseData = await rest(`${categoriesUrl}/params/${categoryId}`, METHODS.GET);
    dispatch({ type: GET_OBYAVA_CATEGORY_PARAMS, payload: responseData });
}

export {
    getCategories,
    getCategory,
    getParamsByCategory
}
