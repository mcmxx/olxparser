import { merge } from 'lodash';
import {
    GET_OFFER_DETAILS,
    GET_OFFER_DETAIL,
    UPDATE_OFFER_DETAIL,
    DELETE_OFFER_DETAIL,
    SWITCH_OFFER_DETIAL,
    LOADED,
    LOADING
} from "../constants/actions";
import { OFFER_DETAILS_URL } from "../constants/urls";
import config from "../config";
import rest from "../utils/rest";
import { METHODS } from "../constants/methods";

const offerDetailsUrl = `${config.backendUrl}${OFFER_DETAILS_URL}`;

const defaultQuery = {
    offset: 0,
    limit: 10,
    search: ''
};

const getOfferDetails = (incomingQuery = {}) => async dispatch => {
    dispatch({ type: LOADING, payload: {} });
    const queryData = merge(defaultQuery, incomingQuery);
    const responseData = await rest(offerDetailsUrl, METHODS.GET, queryData);
    dispatch({ type: GET_OFFER_DETAILS, payload: responseData });
    dispatch({ type: LOADED, payload: {} });
};


const getOfferDetail = id => async dispatch => {
    const responseData = await rest(`${offerDetailsUrl}/${id}`, METHODS.GET);
    dispatch({ type: GET_OFFER_DETAIL, payload: responseData });
}

const updateOfferDetail = (id, detail) => async dispatch => {
    const responseData = await rest(`${offerDetailsUrl}/${id}`, METHODS.PUT, detail);
    dispatch({ type: UPDATE_OFFER_DETAIL, payload: responseData });
}

const deleteOfferDetail = id => async dispatch => {
    const responseData = await rest(`${offerDetailsUrl}/${id}`, METHODS.DELETE);
    dispatch({ type: DELETE_OFFER_DETAIL, payload: responseData });
}

const switchDetail = id => async dispatch => {
    const responseData = await rest(`${offerDetailsUrl}/${id}/switch`, METHODS.PUT);
    dispatch({ type: SWITCH_OFFER_DETIAL, payload: {...responseData, id}});
}

export {
    getOfferDetails,
    getOfferDetail,
    updateOfferDetail,
    deleteOfferDetail,
    switchDetail
};
