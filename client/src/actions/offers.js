import { merge } from 'lodash';
import {
    GET_OFFERS,
    GET_OFFER,
    UPDATE_OFFER,
    DELETE_OFFER,
    EXPORT_OFFERS,
    SET_OFFERS_HEADING,
    SET_OFFERS_CATEGORY,
    GET_OFFER_JSON_CC,
    LOADING,
    LOADED
} from "../constants/actions";
import {
    OFFER_URL,
    OFFERS_URL,
    EXPORT_YANDEX_MARKET_URL
} from "../constants/urls";
import config from "../config";
import rest from "../utils/rest";
import { METHODS } from "../constants/methods";

const offersUrl = `${config.backendUrl}${OFFERS_URL}`;
const offerUrl = `${config.backendUrl}${OFFER_URL}`;

const defaultQuery = {
    offset: 0,
    limit: 10,
    search: ''
};

const getOffers = (incomingQuery = {}) => async dispatch => {
    dispatch({ type: LOADING, payload: {} });
    const queryData = merge(defaultQuery, incomingQuery);
    const responseData = await rest(`${offersUrl}/${incomingQuery.importRequestId}`, METHODS.GET, queryData);
    dispatch({ type: GET_OFFERS, payload: responseData });
    dispatch({ type: LOADED, payload: {} });
};

const getOffer = id => async dispatch => {
    const responseData = await rest(`${offerUrl}/${id}`, METHODS.GET);
    dispatch({ type: GET_OFFER, payload: responseData });
};


const updateOffer = offer => async dispatch => {
    const responseData = await rest(offerUrl, METHODS.PUT, offer);
    dispatch({ type: UPDATE_OFFER, payload: responseData, offer });
};


const deleteOffer = id => async dispatch => {
    await rest(`${offerUrl}/${id}`, METHODS.DELETE);
    dispatch({ type: DELETE_OFFER, payload: { id } });
};

const exportOffers = (importRequestId, offerIds) => async dispatch => {
    const responseData = await rest(
        EXPORT_YANDEX_MARKET_URL,
        METHODS.POST,
        {
            importRequestId,
            offerIds
        }
    );
    dispatch({ type: EXPORT_OFFERS, payload: responseData});
};


const setOffersHeading = (offers, heading) => async dispatch => {
    const response = await rest(`${offersUrl}/heading`, METHODS.PUT, { offers, heading });
    dispatch({ type: SET_OFFERS_HEADING, payload: response });
};

const setOffersCategory = (offers, category) => async dispatch => {
    const response = await rest(`${offersUrl}/category`, METHODS.PUT, { offers, category });
    dispatch({ type: SET_OFFERS_CATEGORY, payload: response });
};

const getOfferJsonCC = id => async dispatch => {
    const response = await rest(`${offersUrl}/cc-export-offers/${id}`, METHODS.GET);
    dispatch({ type: GET_OFFER_JSON_CC, payload: response });
};

export {
    getOffer,
    getOffers,
    updateOffer,
    deleteOffer,
    exportOffers,
    setOffersHeading,
    setOffersCategory,
    getOfferJsonCC
}
