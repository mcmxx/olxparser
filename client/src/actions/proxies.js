import { merge } from 'lodash';
import {
    CREATE_PROXY,
    UPDATE_PROXY,
    GET_PROXIES,
    GET_PROXY,
    DELETE_PROXY,
    RESET_PROXY_FORM,
    SWITCH_PROXY,
    LOADED,
    LOADING
} from "../constants/actions";
import { PROXY_URL } from "../constants/urls";
import config from "../config";
import rest from "../utils/rest";
import { METHODS } from "../constants/methods";

const proxiesUrl = `${config.backendUrl}${PROXY_URL}`;

const defaultQuery = {
    offset: 0,
    limit: 10,
    search: ''
};

const getProxies = (incomingQuery = {}) => async dispatch => {
    dispatch({ type: LOADING, payload: {} });
    const queryData = merge(defaultQuery, incomingQuery);
    const responseData = await rest(proxiesUrl, METHODS.GET, queryData);
    dispatch({ type: GET_PROXIES, payload: responseData });
    dispatch({ type: LOADED, payload: {} });
};


const getProxy = id => async dispatch => {
    const responseData = await rest(`${proxiesUrl}/${id}`, METHODS.GET);
    dispatch({ type: GET_PROXY, payload: responseData });
};


const createProxy = proxy => async dispatch => {
    const responseData = await rest(proxiesUrl, METHODS.POST, proxy);
    dispatch({ type: CREATE_PROXY, payload: responseData });
}


const updateProxy = (id, proxy) => async dispatch => {
    const responseData = await rest(`${proxiesUrl}/${id}`, METHODS.PUT, proxy);
    dispatch({ type: UPDATE_PROXY, payload: responseData });
};

const deleteProxy = id => async dispatch => {
    const responseData = await rest(`${proxiesUrl}/${id}`, METHODS.DELETE);
    dispatch({ type: DELETE_PROXY, payload: responseData });
};

const resetProxyForm = () => async dispatch => {
    dispatch({ type: RESET_PROXY_FORM, payload: {} });
};

const switchProxy = id => async dispatch => {
    const responseData = await rest(`${proxiesUrl}/${id}/switch`, METHODS.PUT);
    dispatch({ type: SWITCH_PROXY, payload: { ...responseData, id } });
}

export {
    getProxies,
    getProxy,
    createProxy,
    updateProxy,
    deleteProxy,
    resetProxyForm,
    switchProxy
};
