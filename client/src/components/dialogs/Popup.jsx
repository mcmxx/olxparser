import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle
} from "@material-ui/core";
import {
    bool,
    func,
    string
} from "prop-types";


function Popup (props) {
    const {
        open,
        title,
        closeFunc,
        children
    } = props;
    return (
        <Dialog
            open={open}
            maxWidth='lg'
            fullWidth={true}
        >
            <DialogTitle id="olx-to-obyava-categories-popup-title">{title}</DialogTitle>
            <DialogContent>
                {children}
            </DialogContent>
            <DialogActions>
                <Button onClick={closeFunc} color="primary">
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
)}

Popup.propTypes = {
    open: bool.isRequired,
    title: string.isRequired,
    closeFunc: func.isRequired
}

export default Popup;
