import React, { Fragment } from "react";
import Confirm from "../confirm";
import Title from "../title";
import {
    Checkbox,
    Table,
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    IconButton,
    Tooltip,
    Grid,
    LinearProgress
} from "@material-ui/core";
import {
    Refresh as RefreshIcon
} from "@material-ui/icons";
import SortingHeader from "../sortingHeader";
import {
    array,
    bool,
    func,
    number,
    string
} from "prop-types";


const PageTableContainer = (
    {
        openConfirm,
        confirmMessage,
        agreeHandler,
        disagreeHandler,
        tableTitle,
        headCells,
        orderBy,
        order,
        sortHandler,
        itemsPerPage,
        currentPage,
        handleChangePage,
        handleChangeRowsPerPage,
        total,
        children,
        isTotalCheckbox,
        currentPageSelectedNums,
        allCheckboxSelectedHandler,
        allIds,
        refreshData,
        progress
    }
) => (
            <Fragment>
                {progress && (<LinearProgress />)}
                <Confirm
                    message={confirmMessage}
                    title="Please, confirm"
                    isOpen={openConfirm}
                    agreeHandler={agreeHandler}
                    disagreeHandler={disagreeHandler}
                />
                <Grid container spacing={1}>
                    <Grid item xs={6}>
                        <Title>{tableTitle}</Title>
                    </Grid>
                    <Grid item xs={6} style={{ textAlign: "right"}}>
                        <IconButton
                            onClick={refreshData}
                        >
                            <Tooltip title="Refresh">
                                <RefreshIcon />
                            </Tooltip>
                        </IconButton>
                    </Grid>
                </Grid>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            {isTotalCheckbox && (
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        indeterminate={currentPageSelectedNums > 0 && currentPageSelectedNums < itemsPerPage && currentPageSelectedNums < total}
                                        checked={total > 0 && (currentPageSelectedNums === itemsPerPage || currentPageSelectedNums === total)}
                                        onChange={() => allCheckboxSelectedHandler(allIds, currentPageSelectedNums)}
                                    />
                                </TableCell>
                            )}
                            {!isTotalCheckbox && (
                                <TableCell></TableCell>
                            )}
                            <SortingHeader
                                headCells={headCells}
                                orderBy={orderBy}
                                order={order}
                                sortHandler={sortHandler}
                                isTotalCheckbox={isTotalCheckbox}
                            />
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {children}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25]}
                                colSpan={headCells.length + 1}
                                count={total}
                                rowsPerPage={itemsPerPage}
                                page={currentPage}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </Fragment>
        );

PageTableContainer.propTypes = {
    openConfirm: bool.isRequired,
    confirmMessage: string.isRequired,
    agreeHandler: func.isRequired,
    disagreeHandler: func.isRequired,
    tableTitle: string.isRequired,
    headCells: array.isRequired,
    orderBy: string.isRequired,
    order: string.isRequired,
    sortHandler: func.isRequired,
    itemsPerPage: number.isRequired,
    currentPage: number.isRequired,
    handleChangePage: func.isRequired,
    handleChangeRowsPerPage: func.isRequired,
    total: number.isRequired,
    isTotalCheckbox: bool,
    currentPageSelectedNums: number,
    allCheckboxSelectedHandler: func,
    allIds: array,
    refreshData: func.isRequired,
    progress: bool.isRequired
};

PageTableContainer.defaultProps = {
    isTotalCheckbox: false,
    currentPageSelectedNums: 0,
    allCheckboxSelectedHandler: undefined,
    allIds: [],
    progress: false
};

export default PageTableContainer;
