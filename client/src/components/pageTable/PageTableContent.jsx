import React from "react";
import {
    Checkbox,
    Link,
    TableCell,
    TableRow,
    Typography
} from "@material-ui/core";
import {
    array,
    func
} from "prop-types";
import { HEAD_CELL_TYPE } from "../../constants/common";
import moment from "moment";

const PageTableContent = props => {
    const {
        data,
        isSelected,
        headCells,
        checkBoxHandler,
        buttonsComponent,
    } = props;
    return data.map((item, index) => {
        const isItemSelected = isSelected && isSelected(item._id);
        let newComponent;


        return (
            <TableRow key={index}>
                {headCells.map((cell, index) => {
                    if(cell.type === HEAD_CELL_TYPE.COMPONENT) {
                        const newProps = {};
                        if(cell.props) {
                            for(let key in cell.props) {
                                newProps[key] = item[cell.props[key]];
                            }
                        }
                        newProps[cell.id] = item[cell.id];
                        newComponent = React.cloneElement(cell.component, newProps)
                    }

                    return (<TableCell
                        key={index}
                        padding={cell.type === HEAD_CELL_TYPE.CHECKBOX ? "checkbox" : "default"}
                        children={cell.type === HEAD_CELL_TYPE.COMPONENT && cell.component}
                    >
                        {cell.type === HEAD_CELL_TYPE.CHECKBOX && (
                            <Checkbox
                                checked={isItemSelected}
                                onChange={() => checkBoxHandler(item[cell.id])}
                            />)}
                        {cell.type === HEAD_CELL_TYPE.LINK && (
                            <Typography>
                                <Link href={item[cell.id]} target='_blank'>
                                    {item[cell.id]}
                                </Link>
                            </Typography>)}

                        {cell.type === HEAD_CELL_TYPE.POPUP_LINK && (
                            <Typography>
                                <Link onClick={() => cell.clickHandler(item._id)}>
                                    {item[cell.id] || "Edit"}
                                </Link>
                            </Typography>
                        )}

                        {cell.type === HEAD_CELL_TYPE.TEXT && (
                            <Typography>
                                {item[cell.id]}
                            </Typography>)}


                        {cell.type === HEAD_CELL_TYPE.DATE && (
                            <Typography>
                                {moment(item[cell.id]).format("DD-MM-YYYY HH:mm")}
                            </Typography>
                        )}

                        {cell.type === HEAD_CELL_TYPE.IMAGE
                        && item.srcImages
                        && item.srcImages.length > 0
                        && <img src={item.srcImages[0]} width="50px" alt={item.title}/>
                        }

                        {cell.type === HEAD_CELL_TYPE.CUSTOM && cell.func(item)}

                        {cell.type === HEAD_CELL_TYPE.COMPONENT && newComponent}
                    </TableCell>)}
                )}
                {buttonsComponent(
                    {
                        item,
                        ...props
                    })}
            </TableRow>
        );
    });
}

PageTableContent.propTypes = {
    data: array.isRequired,
    isSelected: func,
    headCells: array.isRequired,
    checkBoxHandler: func,
    buttonsComponent: func
};


export default PageTableContent;
