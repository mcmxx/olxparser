import React, { Component } from "react";
import { number } from "prop-types";
import ObyavaCategoriesTreeValues from "./ObyavaCategoriesTreeValues";
import rest from "../../../../utils/rest";
import { METHODS } from "../../../../constants/methods";
import { OBYAVA_CATEGORIES_URL } from "../../../../constants/urls";
import config from "../../../../config";

const categoriesUrl = `${config.backendUrl}${OBYAVA_CATEGORIES_URL}`;

class ObyavaCategoriesTree extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: []
        }
    }

    componentDidMount() {
        rest(categoriesUrl, METHODS.GET, { parentId: this.props.parentCategoryId })
            .then(response => {
                this.setState({categories: response.items});
            })
    }

    render() {
        const {
            categories
        } = this.state;

        const nested = this.props.nested + 1;

        return categories.map((category, index) => (
                    <ObyavaCategoriesTreeValues
                        {...this.props}
                        key={index}
                        category={category}
                        nested={nested}
                    />
                ));

    }
}

ObyavaCategoriesTree.propTypes = {
    parentCategoryId: number,
    nested: number
}

ObyavaCategoriesTree.defaultProps = {
    parentCategoryId: null,
    nested: -1
}

export default ObyavaCategoriesTree;
