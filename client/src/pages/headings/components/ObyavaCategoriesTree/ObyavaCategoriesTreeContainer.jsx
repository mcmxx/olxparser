import React from "react";
import {
    List,
    ListSubheader,
    makeStyles
} from "@material-ui/core";
import ObyavaCategoriesTree from "./ObyavaCategoriesTree";

const classes = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 200,
        backgroundColor: theme.palette.background.paper,
    }
}));


export default function ObyavaCategoriesTreeContainer(props) {
    return (
        <List
            component="div"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Obyava Categories
                </ListSubheader>
            }
            className={classes.root}
        >
            <ObyavaCategoriesTree {...props} parentCategoryId={null}/>
        </List>
    );
}

