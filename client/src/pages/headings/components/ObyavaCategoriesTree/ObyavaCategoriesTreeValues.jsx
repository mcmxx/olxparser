import React, {
    Component,
    Fragment
} from "react";
import {
    Collapse,
    ListItem,
    ListItemText,
    List,
    ListItemSecondaryAction,
    withStyles
} from "@material-ui/core";
import {
    ExpandLess,
    ExpandMore
} from "@material-ui/icons";
import {
    func,
    number,
    object
} from "prop-types";
import ObyavaCategoriesTree from "./ObyavaCategoriesTree";

const styles = theme => ({
    nested: {
        paddingLeft: theme.spacing(4),
    }
});

const DIVIDER = ' -- ';

class ObyavaCategoriesTreeValues extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }

    }

    openHandler = () => {
        const open = !this.state.open;
        this.setState({open});
    }

    render() {
        const  {
            category,
            nested,
            classes,
            setCategoryHandler
        } = this.props;

        const { open } = this.state;
        const subs = category.children && Array.isArray(category.children) ? category.children.length : 0;

        return (
            <Fragment>
                <ListItem
                    button
                    className={nested ? classes.nested : ''}
                >
                    <ListItemText
                        primary={nested
                            ? `${DIVIDER.repeat(nested)}${category.name.replace('<br />', '\n')} (${subs})`
                            : `${category.name.replace('<br />', '\n')} ${subs}`}
                        onClick={() => setCategoryHandler(category.id)}
                    />
                    <ListItemSecondaryAction onClick={this.openHandler}>
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItemSecondaryAction>
                </ListItem>
                <Collapse
                    in={open}
                    timeout="auto"
                    unmountOnExit
                >
                    <List component="div" disablePadding>
                        <ObyavaCategoriesTree
                            setCategoryHandler={setCategoryHandler}
                            parentCategoryId={category.id}
                            nested={nested}
                        />
                    </List>
                </Collapse>
            </Fragment>
        );
    }
}

ObyavaCategoriesTreeValues.propTypes = {
    category: object.isRequired,
    nested: number.isRequired,
    setCategoryHandler: func.isRequired
}

ObyavaCategoriesTreeValues.defaultProps = {
}

export default withStyles(styles)(ObyavaCategoriesTreeValues);

