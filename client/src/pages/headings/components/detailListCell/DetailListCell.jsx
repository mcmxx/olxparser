import React, {
    Component,
    Fragment
} from "react";
import {
    Table,
    TableBody,
    TableCell,
    TableRow,
    Link,
    withStyles,
    Collapse,
    Switch,
    Tooltip
} from "@material-ui/core";
import {
    ExpandLess,
    ExpandMore
} from "@material-ui/icons";
import {
    array, bool,
    number
} from "prop-types";
import rest from "../../../../utils/rest";
import { METHODS } from "../../../../constants/methods";
import config from "../../../../config";
import { HEADINGS_URL } from "../../../../constants/urls";
import ValuesListCell from "../valuesListCell/ValuesListCell";

const toggleParameterUrl = `${config.backendUrl}${HEADINGS_URL}/toggleparameter`;
const toggleParameterCategoryUrl = `${config.backendUrl}${HEADINGS_URL}/toggle-parameter-category`;

const styles = theme => ({
    root: {
        whiteSpace: "nowrap"
    },
    inactive: {
        textDecoration: "line-through"
    }
});

class DetailCell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            checked: [],
            category: []
        };
    }

    toggleHandler = (headingId, parameterName) => {
        const { parameters } = this.props;
        rest(`${toggleParameterUrl}`, METHODS.POST, {headingId, parameterName})
            .then(response => {
                const parameter = parameters.find(parameter => parameter.name === parameterName);
                if(parameter) {
                    parameter.active = !parameter.active;
                }
                const index = parameters.findIndex(parameter => parameter.name === parameterName);
                if(index > -1) {
                    const {checked} = this.state
                    checked[index] = !checked[index];
                    this.setState({checked});
                }
            });
    }

    componentDidMount() {
        const { parameters } = this.props;
        const newState = {
            checked: parameters.map(parameter => parameter.active),
            category: parameters.map(parameter => !!parameter.category)
        }
        this.setState(newState);
    }

    toggleCategory = (headingId, parameterName) => {
        const { parameters } = this.props;
        rest(`${toggleParameterCategoryUrl}`, METHODS.POST, {headingId, parameterName})
            .then(response => {
                const parameter = parameters.find(parameter => parameter.name === parameterName);
                if(parameter) {
                    parameter.category = !parameter.category;
                    if(parameter.category) {
                        parameters.forEach(parameter => {
                            if(parameter.name !== parameterName) parameter.category = false;
                        })
                    }
                }
                const index = parameters.findIndex(parameter => parameter.name === parameterName);
                if(index > -1) {
                    let { category } = this.state
                    category[index] = !category[index];
                    if(category[index]) {
                        category = category.map((c, i) => {
                           if(i !== index) c = false;
                           return c;
                        });
                    }
                    this.setState({ category });
                }
            });
    }

    expandHandler = () => {
        const open = !this.state.open;
        this.setState({open});
    }

    render() {
        const {
            parameters,
            onClick,
            categoryId,
            itemId,
            classes,
            isSubParameters
        } = this.props;

        const {
            open
        } = this.state;

        return (
            <Fragment>
                {categoryId && (open
                    ? <Tooltip title="Hide parameters "><ExpandLess onClick={this.expandHandler}/></Tooltip>
                    : <Tooltip title="Show parameters"><ExpandMore onClick={this.expandHandler}/></Tooltip>)}
                <Collapse
                    in={open}
                    timeout="auto"
                    unmountOnExit
                >
                    <Table>
                        <TableBody>
                            {parameters.map((param, index) => {
                                const obyavaParameterName = param.obyavaParameterName || '';
                                return (
                                    <TableRow key={index}>
                                        <TableCell className={classes.root}>
                                            <Link
                                                onClick={() =>
                                                    onClick(itemId, param.name, categoryId, null, null, false, isSubParameters)
                                                }
                                                className={!param.active ? classes.inactive : ''}
                                            >
                                                {`${param.name} :: ${obyavaParameterName}`}
                                            </Link>
                                            <Tooltip title="Enable/Disable parameter">
                                                <Switch
                                                    checked={param.active}
                                                    onChange={() => this.toggleHandler(itemId, param.name)}
                                                    color='primary'
                                                />
                                            </Tooltip>
                                            {!isSubParameters && (
                                                <Tooltip title="Set parameter as category">
                                                    <Switch
                                                    checked={param.category}
                                                    onChange={() => this.toggleCategory(itemId, param.name)}
                                                    color='primary'
                                                    />
                                                </Tooltip>)}
                                        </TableCell>
                                        <TableCell>
                                            <ValuesListCell
                                                values={param.values}
                                                isCategory={param.category}
                                                itemId={itemId}
                                                categoryId={categoryId}
                                                clickFunc={onClick}
                                                parameterName={param.name}
                                                obyavaParameterId={param.obyavaParameterId}
                                                isSubParameters={isSubParameters}
                                            />
                                        </TableCell>
                                    </TableRow>
                                )}
                            )}
                        </TableBody>
                    </Table>
                </Collapse>
            </Fragment>
        );
    }
}

DetailCell.propTypes = {
    itemId: number,
    parameters: array,
    categoryId: number,
    isSubParameters: bool
}

DetailCell.defaultProps = {
    parameters: [],
    categoryId: null,
    isSubParameters: false
}

export default withStyles(styles)(DetailCell);
