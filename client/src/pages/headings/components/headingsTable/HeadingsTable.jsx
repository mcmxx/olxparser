import React, { Component } from "react";
import { connect } from "react-redux";
import {
    array,
    func,
    number,
    bool
} from "prop-types";
import {
    getHeadings,
    deleteHeading
} from "../../../../actions/headings";
import { DELETE_HEADING_CONFIRMATION } from "../../../../constants/notifications";
import {
    HEAD_CELL_TYPE
} from "../../../../constants/common";
import {
    PageTable,
} from "../../../../components/pageTable";
import { HeadingsButtons } from "./index";
import DetailListCell from "../detailListCell";

class HeadingsTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            headingId: undefined
        };
    }

    handleDeleteHeading = headingId => {
        this.setState({
            headingId
        })
    };

    agreeHandler = () => {
        this.props.onDeleteHeading(this.state.headingId);
        this.setState({
            headingId: undefined
        });
    };

    render() {
        const {
            headings,
            total,
            getAll,
            getFilterString,
            getSearchString,
            loading,
            handleOpenObyavaCategoryPopup,
            handleOpenObyavaParamsPopup
        } = this.props;

        const headCells = [
            { id: '_id', numeric: true, disablePadding: true, label: 'id', type: HEAD_CELL_TYPE.TEXT },
            { id: 'heading', numeric: false, disablePadding: true, label: 'Heading', type: HEAD_CELL_TYPE.TEXT},
            {
                id: 'category',
                numeric: false,
                disablePadding: true,
                label: 'Obyava Category',
                type: HEAD_CELL_TYPE.POPUP_LINK,
                clickHandler: handleOpenObyavaCategoryPopup
            },
            {
                id: 'parameters',
                numeric: false,
                disablePadding: true,
                label: 'Details',
                type: HEAD_CELL_TYPE.COMPONENT,
                props: { categoryId: "categoryId", itemId: "_id" },
                component: <DetailListCell onClick={handleOpenObyavaParamsPopup}/>
            },
            { id: 'createdAt', numeric: false, disablePadding: true, label: 'Creating Date', type: HEAD_CELL_TYPE.DATE },
        ];

        return(
            <PageTable
                {...this.props}
                getAll={getAll}
                confirmMessage={DELETE_HEADING_CONFIRMATION}
                agreeHandler={this.agreeHandler}
                tableTitle="Headings"
                headCells={headCells}
                total={total}
                data={headings}
                pageTitle="Headings"
                getFilterString={getFilterString}
                getSearchString={getSearchString}
                buttonsComponent={HeadingsButtons}
                itemDeleteHandler={this.handleDeleteHeading}
                loading={loading}
            />
        );
    }
}

HeadingsTable.propTypes = {
    headings: array.isRequired,
    total: number.isRequired,
    getSearchString: func.isRequired,
    getFilterString: func.isRequired,
    loading: bool,
    handleOpenObyavaCategoryPopup: func.isRequired,
    handleOpenObyavaParamsPopup: func.isRequired
};

HeadingsTable.defaultProps = {
    headings: [],
    total: 0,
    loading: false
};

const mapStateToProps = state => ({
    headings: state.headings.list.items,
    total: state.headings.list.total,
    loading: state.headings.list.loading,
});

const mapDispatchToProps = dispatch => ({
    getAll: (options = {}) => {
        dispatch(getHeadings(options))
    },
    onDeleteHeading: headingId => {
        dispatch(deleteHeading(headingId));
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(HeadingsTable);
