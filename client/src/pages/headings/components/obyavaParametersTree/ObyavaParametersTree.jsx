import React, { Component } from "react";
import {
    bool,
    number
} from "prop-types";
import rest from "../../../../utils/rest";
import { METHODS } from "../../../../constants/methods";
import { OBYAVA_CATEGORIES_URL } from "../../../../constants/urls";
import ObyavaParametersTreeValues from "./ObyavaParametersTreeValues";
import config from "../../../../config";

const obyavaParametersUrl = `${config.backendUrl}${OBYAVA_CATEGORIES_URL}/params`;

class ObyavaParametersTree extends Component {

    constructor(props) {
        super(props);
        this.state = {
            parameters: [],
            is_new_type: 0
        }
    }

    componentDidMount() {
        const {
            categoryId,
            paramId,
            valueId
        } = this.props;

        rest(`${obyavaParametersUrl}/${categoryId}?paramId=${paramId}&valueId=${valueId}`,
            METHODS.GET)
            .then(response => {this.setState({
                parameters: response.category.parameters,
                is_new_type: response.category.is_new_type
            })});
    }

    render() {
        const {
            parameters,
            is_new_type
        } = this.state;
        const nested = this.props.nested + 1;

        return parameters ? parameters.map((parameter, index) => (
           <ObyavaParametersTreeValues
               {...this.props}
               key={index}
               parameter={parameter}
               nested={nested}
               isNewType={is_new_type}
           />
        ))
            : null;
    }
}

ObyavaParametersTree.propTypes = {
    categoryId: number.isRequired,
    parentParameterId: number,
    is_new_type: bool,
    paramId: number,
    valueId: number,
    nested: number
}

ObyavaParametersTree.defaultProps = {
    parentParameterId: null,
    is_new_type: false,
    paramId: null,
    valueId: null,
    nested: -1
}

export default ObyavaParametersTree;
