import React from "react";
import {
    List,
    ListSubheader,
    makeStyles
} from "@material-ui/core";
import ObyavaParametersTree from "./ObyavaParametersTree";

const classes = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 200,
        backgroundColor: theme.palette.background.paper,
    }
}));

export default function ObyavaParametersTreeContainer(props) {
    return (
        <List
            component="div"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Obyava Parameters
                </ListSubheader>
            }
            className={classes.root}
        >
            <ObyavaParametersTree {...props}/>
        </List>
    );
}
