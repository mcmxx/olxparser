import React, {
    Component,
    Fragment
} from "react";
import {
    Collapse,
    ListItem,
    ListItemText,
    ListItemIcon,
    List,
    ListItemSecondaryAction,
    withStyles
} from "@material-ui/core";
import {
    FolderOpen
} from "@material-ui/icons";
import {
    ExpandLess,
    ExpandMore
} from "@material-ui/icons";
import {
    func,
    number,
    object
} from "prop-types";
import ObyavaParametersTree from "./ObyavaParametersTree";
import ObyavaParametersTreeValuesSub from "./ObyavaParametersTreeValuesSub";

const styles = theme => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    value: {
        color: "RED",
        paddingLeft: theme.spacing(4),
    },
    disabled: {
        textDecoration: "line-through"
    }
});

const DIVIDER = ' -- ';

class ObyavaParametersTreeValues extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openValues: false
        }
    }

    openValuesHandler = () => {
        const openValues = !this.state.openValues;
        this.setState({ openValues });
    }


    render() {
        const {
            parameter,
            nested,
            setParameterHandler,
            classes,
            categoryId,
            isNewType
        } = this.props;

        const {
            openValues
        } = this.state;

        return (
            <Fragment>
                <ListItem
                    button
                    className={nested ? classes.nested : ''}
                >
                    <ListItemIcon>
                        <FolderOpen />
                    </ListItemIcon>
                    <ListItemText
                        primary={nested
                            ? `${DIVIDER.repeat(nested)}${parameter.name.replace('<br />', '\n')}`
                            : `${parameter.name.replace('<br />', '\n')}`}
                        onClick={() => setParameterHandler(parameter.id)}
                    />
                    {(!isNewType) && (<ListItemSecondaryAction onClick={this.openValuesHandler}>
                        {openValues ? <ExpandLess /> : <ExpandMore />}
                    </ListItemSecondaryAction>)}
                </ListItem>
                {!isNewType && (<Collapse
                    in={openValues}
                    timeout="auto"
                    unmountOnExit
                >
                    <List component="div" disablePadding>
                        <ObyavaParametersTree
                            categoryId={categoryId}
                            paramId={parameter.id}
                            setParameterHandler={setParameterHandler}
                            nested={nested}
                        />
                        {!isNewType && parameter.values.map((value, index) => (
                            <ObyavaParametersTreeValuesSub
                                key={index}
                                value={value}
                                nested={nested}
                                parameter={parameter}
                                setParameterHandler={setParameterHandler}
                                categoryId={categoryId}
                            />
                        ))}
                    </List>
                </Collapse>)}
            </Fragment>
        );
    }
}

ObyavaParametersTreeValues.propTypes = {
    categoryId: number.isRequired,
    parameter: object.isRequired,
    nested: number.isRequired,
    setParameterHandler: func.isRequired,
    isNewType: number.isRequired
}

ObyavaParametersTreeValues.defaultProps = {

}

export default withStyles(styles)(ObyavaParametersTreeValues);