import React, {
    Component,
    Fragment
} from "react";
import {
    Collapse,
    List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
    withStyles
} from "@material-ui/core";
import {
    FileCopy
} from "@material-ui/icons";
import {
    ExpandLess,
    ExpandMore
} from "@material-ui/icons";
import ObyavaParametersTree from "./ObyavaParametersTree";
import {func, number, object} from "prop-types";

const styles = theme => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    value: {
        color: "RED",
        paddingLeft: theme.spacing(4),
    },
    disabled: {
        textDecoration: "line-through"
    }
});

const DIVIDER = ' -- ';


class ObyavaParametersTreeValuesSub extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    openHandler = () => {
        const open = !this.state.open;
        this.setState({ open });
    }

    render() {
        const {
            value,
            classes,
            nested,
            parameter,
            setParameterHandler,
            categoryId
        } = this.props;

        const {
            open
        } = this.state;

        return (
            <Fragment>
                <ListItem
                    button
                    className={classes.value}
                >
                    <ListItemIcon>
                        <FileCopy />
                    </ListItemIcon>
                    <ListItemText
                        primary={nested
                            ? `${DIVIDER.repeat(nested)}${value.name.replace('<br />', '\n')}`
                            : `${value.name.replace('<br />', '\n')}`}
                    />
                    <ListItemSecondaryAction onClick={this.openHandler}>
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItemSecondaryAction>
                </ListItem>
                <Collapse
                    in={open}
                    timeout="auto"
                    unmountOnExit
                >
                    <List component="div" disablePadding>
                        <ObyavaParametersTree
                            setParameterHandler={setParameterHandler}
                            categoryId={categoryId}
                            paramId={parameter.id}
                            valueId={value.id}
                            nested={nested}
                        />
                    </List>
                </Collapse>
            </Fragment>)
    }
}

ObyavaParametersTreeValuesSub.propTypes = {
    value: object.isRequired,
    parameter: object.isRequired,
    nested: number,
    setParameterHandler: func.isRequired,
    categoryId: number.isRequired
}

ObyavaParametersTreeValuesSub.defaultProps = {
    nested: 0
}

export default withStyles(styles)(ObyavaParametersTreeValuesSub);
