import React, { Component } from "react";
import {
    bool,
    func,
    number
} from "prop-types";
import {
    List,
    ListItem,
    ListItemText
} from "@material-ui/core";
import rest from "../../../../utils/rest";
import { METHODS } from "../../../../constants/methods";
import { OBYAVA_CATEGORIES_URL } from "../../../../constants/urls";
import config from "../../../../config";

const obyavaParametersUrl = `${config.backendUrl}${OBYAVA_CATEGORIES_URL}/values`;

class ObyavaValuesSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            values: [],
            is_new_type: 0
        }
    }

    componentDidMount() {
        const {
            categoryId,
            parameterId
        } = this.props;

        rest(`${obyavaParametersUrl}/${categoryId}/${parameterId}`,
            METHODS.GET)
            .then(response => {
                if(response.values && Array.isArray(response.values)) {
                    this.setState({ values: response.values });
                } else {
                    this.setState({ values: [] });
                }
            });
    }


    render() {
        const { values } = this.state;
        return (
            <List>
                {values.map((value, index) => (
                    <ListItem key={index} onClick={() => this.props.setValueHandler(value.id)}>
                        <ListItemText
                            primary={value.name}
                        />
                    </ListItem>
                ))}
            </List>
        )
    }
}

ObyavaValuesSelector.propTypes = {
    categoryId: number,
    parameterId: number,
    is_new_type: bool,
    setValueHandler: func.isRequired,
}

ObyavaValuesSelector.defaultProps = {
    parameterId: null,
    is_new_type: false,
}

export default ObyavaValuesSelector;
