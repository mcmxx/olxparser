import React, {
    Component,
    Fragment
} from "react";
import {
    List,
    ListItem,
    ListItemText,
    withStyles,
    Collapse,
    Tooltip
} from "@material-ui/core";
import {
    ExpandLess,
    ExpandMore
} from "@material-ui/icons";
import {
    array,
    bool,
    func,
    number,
    string,
    oneOfType
} from "prop-types";
import DetailListCell from "../detailListCell/DetailListCell";

const styles = theme => ({
    root: {
        whiteSpace: "nowrap"
    }
});

class ValuesListCell extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    expandHandler = () => {
        const open = !this.state.open;
        this.setState({open});
    }

    render() {

        const {
            values,
            isCategory,
            itemId,
            parameterName,
            obyavaParameterId,
            categoryId,
            clickFunc,
            classes,
            isSubParameters
        } = this.props;

        const {
            open
        } = this.state;

        return (
            <Fragment>
                {open
                    ? <Tooltip title="Hide values"><ExpandLess onClick={this.expandHandler}/></Tooltip>
                    : <Tooltip title="Show values"><ExpandMore onClick={this.expandHandler}/></Tooltip>}
                <Collapse
                    in={open}
                    timeout="auto"
                    unmountOnExit
                >
                    <List>
                        {values && values.map((value, index) => {
                            let obyavaValueName;
                            if(isCategory)
                                obyavaValueName = value.categoryPath || '';
                            else
                                obyavaValueName = value.obyavaValueName || '';
                            return (
                                <ListItem
                                    key={index}
                                >
                                    <ListItemText
                                        onClick={() => clickFunc(
                                            itemId,
                                            parameterName,
                                            categoryId,
                                            value.name,
                                            obyavaParameterId,
                                            isCategory,
                                            isSubParameters
                                        )}
                                        primary={`${value.name} :: ${obyavaValueName}`}
                                        className={classes.root}
                                    />
                                    {value.parameters &&
                                        <DetailListCell
                                            itemId={itemId}
                                            categoryId={value.categoryId}
                                            parameters={value.parameters}
                                            isSubParameters={true}
                                            onClick={clickFunc}
                                        />
                                    }
                                </ListItem>
                            )})}
                    </List>
                </Collapse>
            </Fragment>
        );
    }
}

ValuesListCell.propTypes = {
    values: array.isRequired,
    itemId: oneOfType([string, number]).isRequired,
    parameterName: string.isRequired,
    categoryId: number.isRequired,
    obyavaParameterId: number,
    isCategory: bool.isRequired,
    clickFunc: func.isRequired,
    isSubParameters: bool
}

ValuesListCell.defaultProps = {
    values: [],
    isSubParameters: false,
    obyavaParameterId: null
}

export default withStyles(styles)(ValuesListCell);
