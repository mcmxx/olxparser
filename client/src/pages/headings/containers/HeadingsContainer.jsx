import React,
{
    Component,
    Fragment
} from "react";
import { connect } from "react-redux";
import {
    Container,
    Grid,
    Paper,
    Button,
    Select,
    InputLabel,
    MenuItem,
    FormControl,
    withStyles
} from "@material-ui/core";
import HeadingsTable from "../components/headingsTable";
import ListItemLink from "../../../components/listItemLink";
import Search from "../../../components/search/Search";
import { CREATE_HEADING_PAGE_PATH } from "../../../constants/router";
import { headingsFilterItems } from "../../../constants/common";
import {
    setHeadingCategoryAssoc,
    resetHeadingCategoryAssoc,
    setHeadingParameterAssoc,
    setHeadingValueAssoc
} from "../../../actions/headings";
import ObyavaCategoriesTreeContainer from "../components/ObyavaCategoriesTree";
import ObyavaParametersTreeContainer from "../components/obyavaParametersTree";
import ObyavaValuesSelector from "../components/obyavaValuesSelector";
import Popup from "../../../components/dialogs";

const styles = theme => ({
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    button: {
        margin: theme.spacing(1)
    },
    formControl: {
        margin: theme.spacing(1.5),
        minWidth: 200,
    },
});

class HeadingsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            filter: headingsFilterItems[0].value,
            currentHeading: null,
            openOlxToObyavaCategoriesPopup: false,
            refreshTrigger: false,
            openObyavaParamsPopup: false,
            categoryId: null,
            parameterName: null,
            openOlxToObyavaValuesPopup: false,
            valueName: null,
            obyavaParameterId: null,
            parameterId: null,
            openParameterToCategoryPopup: false,
            isSubParameter: false
        }
    }

    handleCloseParameterToCategoryPopUp = () => {
        this.setState({
            openParameterToCategoryPopup: false,
        });
    }

    handleClickOlxParamValueField = (
        headingId,
        categoryId,
        obyavaParameterId,
        valueName,
        categoryFlag = false,
    ) => {
        if(categoryFlag) {
            this.setState({
                openParameterToCategoryPopup: true,
                categoryId,
                valueName,
                currentHeading: headingId,
                obyavaParameterId
            });
        } else {
            this.setState({
                openOlxToObyavaValuesPopup: true,
                categoryId,
                valueName,
                currentHeading: headingId,
                obyavaParameterId
            });
        }
    }

    handleCLoseOlxParamValuesPopup = () => {
        this.setState({
            openOlxToObyavaValuesPopup: false,
            isSubParameter: false
        })
    }

    handleClickOlxParamField = (
        headingId,
        parameterName,
        categoryId,
        valueName = null,
        parameterId = null,
        isCategory = false,
        isSubParameter = false
    ) => {
        if(isSubParameter) {
            if(valueName && parameterId) {
                this.handleClickOlxValueField(headingId, parameterName, categoryId, valueName, parameterId, isSubParameter);
            } else {
                this.setState({
                    openObyavaParamsPopup: true,
                    categoryId,
                    parameterName,
                    currentHeading: headingId,
                    isSubParameter
                });
            }
            return;
        }

        if(isCategory) {
            this.setState({
                openParameterToCategoryPopup: true,
                categoryId,
                parameterName,
                valueName,
                currentHeading: headingId
            });
        } else if(valueName && parameterId) {
            this.handleClickOlxValueField(headingId, parameterName, categoryId, valueName, parameterId);
        } else {
            this.setState({
                openObyavaParamsPopup: true,
                categoryId,
                parameterName,
                currentHeading: headingId
            });
        }
    }

    handleClickOlxValueField = (headingId, parameterName, categoryId, valueName, parameterId, isSubParameter = false) => {
        this.setState({
            openOlxToObyavaValuesPopup: true,
            categoryId,
            parameterName,
            currentHeading: headingId,
            valueName,
            parameterId,
            isSubParameter
        });
    }

    handleCloseObyavaParameterPopUp = () => {
        this.setState({
            openObyavaParamsPopup: false,
            isSubParameter: false
        });
    }

    handleClickObyavaCategoryField = (itemId) => {
        this.setState({
            openOlxToObyavaCategoriesPopup: true,
            currentHeading: itemId
        });
    }

    handleCloseObyavaCategoryPopup = () => {
        this.setState({
            openOlxToObyavaCategoriesPopup: false,
            currentHeading: null
        });
    }

    onChangeSearchHandler = (e) => {
        const refreshTrigger = !this.state.refreshTrigger;
        this.setState({
            search: e.target.value,
            refreshTrigger
        });
    };

    getSearchString = () => {
        return this.state.search;
    };

    getFilterString = () => {
        return this.state.filter;
    };

    onChangeFilterHandler = (e) => {
        const refreshTrigger = !this.state.refreshTrigger;
        this.setState({
            refreshTrigger,
            filter: e.target.value
        });
    };

    handleParametersAssoc = (obyavaParameterId) => {
        const {
            onSetHeadingParameterAssoc
        } = this.props;
        const {
            currentHeading: headingId,
            categoryId,
            parameterName,
            isSubParameter
        } = this.state
        const refreshTrigger = !this.state.refreshTrigger;

        if(obyavaParameterId) {
            onSetHeadingParameterAssoc(headingId,
                categoryId,
                parameterName,
                obyavaParameterId,
                isSubParameter);
        }

        this.setState({refreshTrigger});
        this.handleCloseObyavaParameterPopUp();
    }

    handleHeadingCategoryAssoc = (categoryId) => {
        const {
            onSetHeadingsCategoriesAssoc,
        } = this.props;
        const headingId = this.state.currentHeading;
        const refreshTrigger = !this.state.refreshTrigger;

        if(categoryId) {
            onSetHeadingsCategoriesAssoc(headingId, categoryId);
        }

        this.setState({refreshTrigger});
        this.handleCloseObyavaCategoryPopup();
    }

    handleParameterToCategoryAssoc = (categoryId) => {
        const {
            onSetHeadingValueAssoc
        } = this.props;
        const {
            currentHeading: headingId,
            parameterName,
            valueName
        } = this.state
        const refreshTrigger = !this.state.refreshTrigger;

        if(categoryId) {
            onSetHeadingValueAssoc(
                headingId,
                categoryId,
                parameterName,
                valueName,
                null,
                true
            )
        }

        this.setState({ refreshTrigger });
        this.handleCloseParameterToCategoryPopUp();
    }

    handleHeadingValueAssoc = (valueId) => {
        const {
            onSetHeadingValueAssoc
        } = this.props;
        const {
            currentHeading: headingId,
            categoryId,
            parameterName,
            valueName,
            isSubParameter
        } = this.state
        const refreshTrigger = !this.state.refreshTrigger;

        if(valueId) {
            onSetHeadingValueAssoc(
                headingId,
                categoryId,
                parameterName,
                valueName,
                valueId,
                false,
                isSubParameter
            )
        }

        this.setState({ refreshTrigger });
        this.handleCLoseOlxParamValuesPopup();
    }

    render() {
        const { classes } = this.props;
        const {
            filter,
            openOlxToObyavaCategoriesPopup,
            openObyavaParamsPopup,
            refreshTrigger,
            categoryId,
            openOlxToObyavaValuesPopup,
            parameterId,
            openParameterToCategoryPopup
        } = this.state;

        return (
            <Fragment>
                <Popup
                    open={openParameterToCategoryPopup}
                    title="Select Category from Obyava"
                    closeFunc={this.handleCloseParameterToCategoryPopUp}
                >
                    <ObyavaCategoriesTreeContainer
                        categoryId={null}
                        setCategoryHandler={this.handleParameterToCategoryAssoc}/>
                </Popup>
                <Popup
                    open={openOlxToObyavaValuesPopup}
                    title="Select Values from Obyava"
                    closeFunc={this.handleCLoseOlxParamValuesPopup}
                >
                    <ObyavaValuesSelector
                        categoryId={categoryId}
                        parameterId={parameterId}
                        setValueHandler={this.handleHeadingValueAssoc}
                    />
                </Popup>
                <Popup
                    open={openObyavaParamsPopup}
                    title="Select Parameter from Obyava"
                    closeFunc={this.handleCloseObyavaParameterPopUp}
                >
                    <ObyavaParametersTreeContainer
                        setParameterHandler={this.handleParametersAssoc}
                        categoryId={categoryId}
                    />
                </Popup>
                <Popup
                    open={openOlxToObyavaCategoriesPopup}
                    title="Select Category from Obyava"
                    closeFunc={this.handleCloseObyavaCategoryPopup}
                >
                    <ObyavaCategoriesTreeContainer
                        categoryId={null}
                        setCategoryHandler={this.handleHeadingCategoryAssoc}/>
                </Popup>
                <Container maxWidth="xl" className={classes.container}>
                    <Grid container spacing={3}>
                        <Grid item xs={3}/>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel id="filter-select">Filter</InputLabel>
                            <Select
                                id="filter-select"
                                onChange={this.onChangeFilterHandler}
                                labelWidth={32}
                                value={filter}
                            >
                                {headingsFilterItems.map(item => (
                                    <MenuItem key={item.value} value={item.value}>{item.option}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <Search onChange={this.onChangeSearchHandler}/>
                        <Grid item xs={2}>
                            <Button
                                variant="contained"
                                color="primary"
                                size="medium"
                                className={classes.button}
                                component={ListItemLink}
                                to={CREATE_HEADING_PAGE_PATH}
                            >
                                Create Heading
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <HeadingsTable
                                    getSearchString={this.getSearchString}
                                    getFilterString={this.getFilterString}
                                    handleOpenObyavaCategoryPopup={this.handleClickObyavaCategoryField}
                                    handleOpenObyavaParamsPopup={this.handleClickOlxParamField}
                                    refreshTrigger={refreshTrigger}
                                />
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onSetHeadingsCategoriesAssoc: (headingId, categoryId) => {
        dispatch(setHeadingCategoryAssoc(headingId, categoryId));
    },
    onResetHeadingsCategoriesAssoc: id => {
        dispatch(resetHeadingCategoryAssoc(id));
    },
    onSetHeadingParameterAssoc: (
            headingId,
            categoryId,
            parameterName,
            obyavaParameterId,
            isSubParameter = false
        ) => {
            dispatch(setHeadingParameterAssoc(
                headingId,
                categoryId,
                parameterName,
                obyavaParameterId,
                isSubParameter
            ));
        },
    onSetHeadingValueAssoc: (
            headingId,
            categoryId,
            parameterName,
            valueName,
            valueId,
            categoryFlag = false,
            isSubParameter = false
        ) => {
            dispatch(setHeadingValueAssoc(
                headingId,
                categoryId,
                parameterName,
                valueName,
                valueId,
                categoryFlag,
                isSubParameter
            ));
        }
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(HeadingsContainer));
