import { isNil } from "lodash";
import {
    DELETE_HEADING,
    GET_HEADINGS,
    LOADING,
    LOADED
} from "../../../constants/actions";

const initialState = {
    items: []
};

export default function list(state = initialState, action) {
    const {
        type,
        payload
    } = action;

    if(action.type === LOADING) {
        return {
            ...state,
            loading: true
        }
    }

    if(action.type === LOADED) {
        return {
            ...state,
            loading: false
        }
    }

    if(type === GET_HEADINGS) {
        payload.items = payload.items.map(item => {
            if(isNil(item.category)) return item;
            item.categoryId = item.category.id;
            item.category = item.category.path;
            return item;
        })
        return payload;
    }

    if(type === DELETE_HEADING) {
        const newItems = state.items.filter((item) => {
            return item._id !== payload.id;
        });
        return {
            items: newItems
        }
    }

    return state;
}