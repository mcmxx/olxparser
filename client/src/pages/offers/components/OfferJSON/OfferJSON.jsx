import React, { Component } from "react";
import { connect } from "react-redux";
import {
    object,
    string,
    func, bool
} from "prop-types";
import {
    getOfferJsonCC
} from "../../../../actions/offers";
import Alert from "../../../../components/alert";

class OfferJSON extends Component {

    componentDidMount() {
        const {
            id,
            onGetOfferJsonCC
        } = this.props;
        if(id) {
            onGetOfferJsonCC(id);
        }
    }

    render() {
        const {
            bundle,
            closeOfferJSONHandler,
            isOpen
        } = this.props;

        return (
            <Alert
                key={bundle}
                message={bundle ? JSON.stringify(bundle) : ''}
                title="Output JSON"
                closeHandler={closeOfferJSONHandler}
                isOpen={isOpen}
            />
        )
    }
}

OfferJSON.propTypes = {
    id: string.isRequired,
    bundle: object,
    closeOfferJSONHandler: func.isRequired,
    isOpen: bool.isRequired
}

OfferJSON.defaultProps = {
    id: '',
    bundle: {}
}

const mapStateToProps = state => ({
    bundle: state.offers.list.bundle
});

const mapDispatchToProps = dispatch => ({
    onGetOfferJsonCC: id => {
        dispatch(getOfferJsonCC(id))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(OfferJSON);
