import React,
{
    Component,
    Fragment
} from "react";
import { connect } from "react-redux";
import {
    string,
    array,
    number,
    object,
    func, bool
} from "prop-types";
import {
    getOffers,
    deleteOffer
} from "../../../../actions/offers";
import { DELETE_OFFER_CONFIRMATION } from "../../../../constants/notifications";
import {
    HEAD_CELL_TYPE,
    OFFER_CATEGORY_TYPE
} from "../../../../constants/common";
import { PageTable } from "../../../../components/pageTable";
import OffersButtons from "./OffersButtons";
import OfferJSON from "../OfferJSON/OfferJSON";

const categoryTypeRendering = ({ categoryType }) => {
    switch (categoryType) {
        case OFFER_CATEGORY_TYPE.CONVERTED:
            return "Will convert";

        case OFFER_CATEGORY_TYPE.OBYAVA:
            return "Obyava.UA";

        default:
        case OFFER_CATEGORY_TYPE.OLX:
            return "OLX";
    }
};

const headCells = [
    { id: '_id', numeric: false, disablePadding: true, label: '', type: HEAD_CELL_TYPE.CHECKBOX },
    { id: 'title', numeric: false, disablePadding: true, label: 'Title', type: HEAD_CELL_TYPE.TEXT },
    { id: 'headingString', numeric: false, disablePadding: true, label: 'Heading', type: HEAD_CELL_TYPE.TEXT },
    { id: 'categoryName', numeric: false, disablePadding: true, label: 'Category', type: HEAD_CELL_TYPE.TEXT },
    {
        id: 'categoryType',
        numeric: false,
        disablePadding: true,
        label: 'CType',
        type: HEAD_CELL_TYPE.CUSTOM,
        func: categoryTypeRendering
    }
    //{ id: 'description', numeric: false, disablePadding: true, label: 'Description', type: HEAD_CELL_TYPE.TEXT }
];

class OffersTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            offerId: undefined,
            getOferJsonCCAlertOpen: false
        };
    }

    agreeHandler = () => {
        this.props.onDeleteOffer(this.state.offerId);
        this.setState({
            offerId: undefined
        });
    };

    handleDeleteOffer = id => {
        this.setState({
            offerId: id
        });
    };

    handleGetOfferJsonCC = id => {
        this.setState({
            getOferJsonCCAlertOpen: true,
            offerId: id
        });
    }

    handleCloseGetOfferJsonCCAlert = () => {
        this.setState({
            getOferJsonCCAlertOpen: false,
            offerId: undefined
        });
    }

    isSelected = (id) => {
        const { selectedItems } = this.props;
        return selectedItems.includes(id);
    };

    calculateNumSelectedOnCurrentPage = () => {
        const {
            selectedItems,
            offers
        } = this.props;

        return offers.reduce((acc, offer) => {
            if(selectedItems.includes(offer._id)) {
                acc++;
            }

            return acc;
        }, 0);
    };

    render() {
        const {
            offers,
            total,
            importRequest,
            offerCheckBoxHandler,
            offerCheckBoxSelectAllHandler,
            getAll,
            getSearchString,
            importRequestId,
            loading
        } = this.props;

        let pageTitle = 'Offers';
        if(importRequest.email) {
            pageTitle = `Offers for ${importRequest.email} account`;
        } else if(importRequest.sessionId) {
            pageTitle = `Offers for call center session # ${importRequest.sessionId}`;
        }
        const params = { importRequestId }
        const allIds = offers.map(offer => offer._id);
        const currentPageSelectedNums = this.calculateNumSelectedOnCurrentPage();

        return (
            <Fragment>
                <OfferJSON
                    key={this.state.offerId}
                    id={this.state.offerId}
                    isOpen={this.state.getOferJsonCCAlertOpen}
                    closeOfferJSONHandler={this.handleCloseGetOfferJsonCCAlert}
                />
                <PageTable
                    key={pageTitle}
                    getAll={getAll}
                    confirmMessage={DELETE_OFFER_CONFIRMATION}
                    agreeHandler={this.agreeHandler}
                    tableTitle="Offers"
                    headCells={headCells}
                    total={total}
                    data={offers}
                    pageTitle={pageTitle}
                    getFilterString={() => ""}
                    getSearchString={getSearchString}
                    currentPageSelectedNums={currentPageSelectedNums}
                    allIds={allIds}
                    allCheckboxSelectedHandler={offerCheckBoxSelectAllHandler}
                    checkBoxHandler={offerCheckBoxHandler}
                    isItemSelected={this.isSelected}
                    queryParams={params}
                    buttonsComponent={OffersButtons}
                    itemDeleteHandler={this.handleDeleteOffer}
                    isTotalCheckbox={true}
                    jsonExampleHandler={this.handleGetOfferJsonCC}
                    loading={loading}
                    {...this.props}
                />
            </Fragment>
        );
    }
}

OffersTable.propTypes = {
    importRequestId: string.isRequired,
    offers: array.isRequired,
    total: number.isRequired,
    importRequest: object.isRequired,
    getSearchString: func.isRequired,
    offerCheckBoxHandler: func.isRequired,
    offerCheckBoxSelectAllHandler: func.isRequired,
    selectedItems: array.isRequired,
    loading: bool
};

OffersTable.defaultProps = {
    offers: [],
    total: 0,
    importRequestId: '',
    importRequest: {},
    loading: false
};

const mapStateToProps = state => ({
    offers: state.offers.list.items,
    total: state.offers.list.total,
    importRequest: state.offers.list.importRequest,
    loading: state.offers.list.loading
});

const mapDispatchToProps = dispatch => ({
    getAll: (options = {}) => {
        dispatch(getOffers(options));
    },
    onDeleteOffer: id => {
        dispatch(deleteOffer(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(OffersTable);
