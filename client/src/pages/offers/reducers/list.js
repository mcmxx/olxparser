import {
    DELETE_OFFER,
    GET_OFFERS,
    GET_OFFER_JSON_CC,
    LOADED,
    LOADING,
} from "../../../constants/actions";

const initialState = {
    items: [],
    bundle: {},
    loading: true
};

export default function list(state = initialState, action) {
    const {
        type,
        payload
    } = action;

    if(action.type === LOADING) {
        return {
            ...state,
            loading: true
        }
    }

    if(action.type === LOADED) {
        return {
            ...state,
            loading: false
        }
    }

    if (type === GET_OFFERS) {
        return {
            ...state,
            items: payload.items,
            total: payload.total
        };
    }

    if (type === DELETE_OFFER) {
        const newItems = state.items.filter((item) => {
            return item._id !== payload.id;
        });
        return {
            items: newItems
        }
    }

    if(type === GET_OFFER_JSON_CC) {
        return {
            ...state,
            bundle: payload.bundle
        }
    }

    return state;
}