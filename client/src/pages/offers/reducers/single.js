import {
    GET_OFFER,
    SET_HEADING_CATEGORY_ASSOCIATION,
    RESET_HEADING_CATEGORY_ASSOCIATION
} from "../../../constants/actions";

const initialState = {
    t: Date.now()
};

export default function single(state = initialState, action) {
    if(action.type === GET_OFFER) {
        return {
            ...state,
            ...action.payload.item,
            t: Date.now(),
            loaded: true
        }
    }

    if(action.type === SET_HEADING_CATEGORY_ASSOCIATION) {
        return {
            ...state,
            t: Date.now()
        }
    }


    if(action.type === RESET_HEADING_CATEGORY_ASSOCIATION) {
        return {
            ...state,
            t: Date.now()
        }
    }

    return state;

}
