import React from "react";
import {
    IconButton,
    TableCell,
    Tooltip
} from "@material-ui/core";
import { EDIT_PROXY_PAGE_PATH } from "../../../../constants/router";
import ListItemLink from "../../../../components/listItemLink";
import {
    Delete as DeleteIcon,
    Edit as EditIcon,
    Block as BlockIcon,
    CheckCircleOutline as UnblockIcon
} from "@material-ui/icons";
import {
    func,
    object
} from "prop-types";

const ProxiesButtons = ({ item, deleteHandler, switchHandler }) => (
    <TableCell>
        <IconButton onClick={() => switchHandler(item._id)}>
            {item.active && (
                <Tooltip title="Press to disable">
                    <UnblockIcon />
                </Tooltip>
            )}
            {!item.active && (
                <Tooltip title="Press to enable">
                    <BlockIcon />
                </Tooltip>
            )}
        </IconButton>
        <IconButton to={`${EDIT_PROXY_PAGE_PATH}/${item._id}`} component={ListItemLink}>
            <Tooltip title="Edit proxy">
                <EditIcon />
            </Tooltip>
        </IconButton>
        <IconButton onClick={() => deleteHandler(item._id)}>
            <Tooltip title="Delete proxy">
                <DeleteIcon />
            </Tooltip>
        </IconButton>
    </TableCell>
);

ProxiesButtons.propTypes = {
    item: object.isRequired,
    deleteHandler: func.isRequired,
    switchHandler: func.isRequired
};

export default ProxiesButtons;
