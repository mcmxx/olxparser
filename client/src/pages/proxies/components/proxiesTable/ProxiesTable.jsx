import React, { Component } from "react";
import { connect } from "react-redux";
import {
    array,
    func,
    number,
    bool
} from "prop-types";
import {
    getProxies,
    deleteProxy,
    switchProxy
} from "../../../../actions/proxies";
import { DELETE_PROXY_CONFIRMATION } from "../../../../constants/notifications";
import { HEAD_CELL_TYPE } from "../../../../constants/common";
import {
    PageTable,
} from "../../../../components/pageTable";
import { ProxiesButtons } from "./index";

const headCells = [
    { id: 'url', numeric: false, disablePadding: true, label: 'URL', type: HEAD_CELL_TYPE.TEXT },
    { id: 'port', numeric: false, disablePadding: true, label: 'Port', type: HEAD_CELL_TYPE.TEXT },
    { id: "type", numeric: false, disablePadding: true, label: "Type", type: HEAD_CELL_TYPE.TEXT },
    { id: 'username', numeric: false, disablePadding: true, label: 'User', type: HEAD_CELL_TYPE.TEXT },
    { id: 'password', numeric: false, disablePadding: true, label: 'Password', type: HEAD_CELL_TYPE.TEXT }
];

class ProxiesTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            proxyId: undefined
        };
    }

    handleDeleteProxy = proxyId => {
        this.setState({
            proxyId
        })
    };

    switchHandler = proxyId => {
        this.props.onSwitchProxy(proxyId);
    }

    agreeHandler = () => {
        this.props.onDeleteProxy(this.state.proxyId);
        this.setState({
            proxyId: undefined
        });
    };

    render() {
        const {
            proxies,
            total,
            getAll,
            getFilterString,
            getSearchString,
            loading
        } = this.props;

        return(
            <PageTable
                getAll={getAll}
                confirmMessage={DELETE_PROXY_CONFIRMATION}
                agreeHandler={this.agreeHandler}
                tableTitle="Proxies"
                headCells={headCells}
                total={total}
                data={proxies}
                pageTitle="Proxies"
                getFilterString={getFilterString}
                getSearchString={getSearchString}
                buttonsComponent={ProxiesButtons}
                itemDeleteHandler={this.handleDeleteProxy}
                switchHandler={this.switchHandler}
                loading={loading}
                {...this.props}
            />
        );
    }
}

ProxiesTable.propTypes = {
    proxies: array.isRequired,
    total: number.isRequired,
    getSearchString: func.isRequired,
    getFilterString: func.isRequired,
    loading: bool
}

ProxiesTable.defaultProps = {
    proxies: [],
    total: 0,
    loading: false
}

const mapStateToProps = state => ({
    proxies: state.proxies.list.items,
    total: state.proxies.list.total,
    loading: state.proxies.list.loading
});

const mapDispatchToProps = dispatch => ({
    getAll: (options = {}) => {
        dispatch(getProxies(options))
    },
    onDeleteProxy: proxyId => {
        dispatch(deleteProxy(proxyId))
    },
    onSwitchProxy: proxyId => {
        dispatch(switchProxy(proxyId))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProxiesTable);
