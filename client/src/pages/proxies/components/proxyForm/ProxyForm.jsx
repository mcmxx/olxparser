import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import {
    Button,
    TextField,
    withStyles,
    Select
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import {
    bool,
    number,
    string
} from "prop-types";
import { isNil } from "lodash";
import {
    createProxy,
    updateProxy,
    getProxy,
    resetProxyForm
} from "../../../../actions/proxies";
import { PROXIES_PAGE_PATH } from "../../../../constants/router";
import { menuClick } from "../../../../actions/menu";
import { proxyTypeItems } from "../../../../constants/common";

const styles = theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 500,
    },
    selectField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 500,
    },
    button: {
        margin: theme.spacing(1),
    },
});

class ProxyForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            url: undefined,
            port: undefined,
            username: undefined,
            password: undefined,
            type: undefined,
            cancel: false,
            submitted: false
        }
    }

    componentDidMount() {
        const {
            proxyId,
            onCreateTitle,
            onGetProxy,
            resetForm
        } = this.props;

        resetForm();

        const newState = {
            url: undefined,
            redirect: false,
            port: undefined,
            username: undefined,
            password: undefined,
            type: undefined,
            cancel: false
        };
        this.setState(newState);

        if(proxyId) {
            onCreateTitle("Edit Proxy");
            onGetProxy(proxyId);
        } else {
            onCreateTitle("Create Proxy");
        }
    }

    handleFieldChange = (e, field) => {
        this.setState({ [field]: e.target.value });
    }

    renderRedirect = () => {
        if(this.state.redirect) {
            return <Redirect to={PROXIES_PAGE_PATH}/>;
        }
    };

    cancelHandler = (event) => {
        event.preventDefault();
        const newState = {
            url: undefined,
            port: undefined,
            username: undefined,
            password: undefined,
            type: undefined,
            redirect: true,
            cancel: true
        };
        this.setState(newState);
    };

    proxySubmitHandler = (event) => {
        event.preventDefault();
        const url = this.state.url ? this.state.url : this.props.url;
        const port = this.state.port ? this.state.port : this.props.port;
        const username = !isNil(this.state.username) ? this.state.username : this.props.username;
        const password = !isNil(this.state.password) ? this.state.password : this.props.password;
        const type = !isNil(this.state.type) ? this.state.type : this.props.type;
        this.props.saveProxy(
            this.props.proxyId,
            {
                url,
                port,
                username,
                password,
                type
            }
        );
        const newState = {
            redirect: true,
            cancel: false
        };
        this.setState(newState);
    }

    render() {
        let {
            proxyId,
            classes,
            url,
            port,
            username,
            password,
            type,
            t,
            error,
            loaded
        } = this.props;

        if(!proxyId) {
            url = port = username = password = undefined;
        }

        return (
            <Fragment key={t}>
                {(!loaded && error) && (<Alert severity="error">Proxy already exists!</Alert>)}
                {((!error && loaded) || this.state.cancel) && this.renderRedirect()}
                <form onSubmit={this.proxySubmitHandler}>
                    <Select
                        native
                        value={type}
                        label="Proxy Type"
                        inputProps={{
                            name: "type",
                            id: "proxyType"
                        }}
                        className={classes.selectField}
                        onChange={event => this.handleFieldChange(event, 'type')}
                    >
                        {proxyTypeItems.map(item => (
                            <option value={item.value}>{item.option}</option>
                        ))}
                    </Select>
                    <TextField
                        id="url"
                        label="URL"
                        className={classes.textField}
                        margin="normal"
                        required
                        onChange={event => this.handleFieldChange(event, 'url')}
                        defaultValue={url}
                        InputLabelProps={{shrink: true}}
                    />
                    <TextField
                        id="port"
                        label="Port"
                        className={classes.textField}
                        margin="normal"
                        onChange={event => this.handleFieldChange(event, 'port')}
                        defaultValue={port}
                        InputLabelProps={{shrink: true}}
                    />
                    <TextField
                        id="username"
                        label="User"
                        className={classes.textField}
                        margin="normal"
                        onChange={event => this.handleFieldChange(event, 'username')}
                        defaultValue={username}
                        InputLabelProps={{shrink: true}}
                    />
                    <TextField
                        id="password"
                        label="Password"
                        className={classes.textField}
                        margin="normal"
                        onChange={event => this.handleFieldChange(event, 'password')}
                        defaultValue={password}
                        InputLabelProps={{shrink: true}}
                    />
                    <div style={{textAlign: "right"}}>
                        <Button variant="contained" className={classes.button} onClick={this.cancelHandler}>Cancel</Button>
                        <Button type="submit" variant="contained" color="primary" className={classes.button}>Save</Button>
                    </div>
                </form>
            </Fragment>
        )
    }
}

ProxyForm.propTypes = {
    url: string,
    port: string,
    username: string,
    password: string,
    t: number,
    proxyId: string,
    error: bool,
    loaded: bool
};

ProxyForm.defaultProps = {
    url: undefined,
    port: undefined,
    username: undefined,
    password: undefined,
    t: 0,
    headingId: undefined,
    error: false,
    loaded: false
}

const mapStateToProps = state => ({
    url: state.proxies.single.url,
    port: state.proxies.single.port,
    username: state.proxies.single.username,
    password: state.proxies.single.password,
    t: state.proxies.single.t,
    error: state.proxies.single.error,
    loaded: state.proxies.single.loaded
});

const mapDispatchToProps = dispatch => ({
    saveProxy: (proxyId, proxy) => {
        if(proxyId) {
            dispatch(updateProxy(proxyId, proxy));
        } else {
            dispatch(createProxy(proxy));
        }
    },
    onGetProxy: id => {
        dispatch(getProxy(id));
    },
    onCreateTitle: title => {
        dispatch(menuClick(title));
    },
    resetForm: () => {
        dispatch(resetProxyForm());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProxyForm));
