import React, { Component } from "react";
import {
    Container,
    Grid,
    Paper,
    Button,
    withStyles
} from "@material-ui/core";
import ProxiesTable from "../components/proxiesTable";
import ListItemLink from "../../../components/listItemLink";
import Search from "../../../components/search/Search";
import {CREATE_PROXY_PAGE_PATH} from "../../../constants/router";

const styles = theme => ({
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    button: {
        margin: theme.spacing(1)
    }
});

class ProxiesContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            refreshTrigger: false
        }
    }

    onChangeSearchHandler = (e) => {
        const refreshTrigger = !this.state.refreshTrigger;
        this.setState({
            search: e.target.value,
            refreshTrigger
        });
    };

    getSearchString = () => {
        return this.state.search;
    };

    render() {
        const { classes } = this.props;
        const { refreshTrigger } = this.state;

        return (
            <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3}>
                    <Grid item xs={6}/>
                    <Search onChange={this.onChangeSearchHandler}/>
                    <Grid item xs={2}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="medium"
                            className={classes.button}
                            component={ListItemLink}
                            to={CREATE_PROXY_PAGE_PATH}
                        >
                            Create Proxy
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <ProxiesTable
                                getSearchString={this.getSearchString}
                                getFilterString={() => {}}
                                refreshTrigger={refreshTrigger}
                            />
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        )
    }
}

export default withStyles(styles)(ProxiesContainer);
