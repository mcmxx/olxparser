import React  from "react";
import FormContainer from "../../../components/formContainer";
import ProxyForm from "../components/proxyForm/ProxyForm";

const ProxyFormContainer = props => {
    const proxyId = props.match.params.proxyId;

    return (
        <FormContainer>
            <ProxyForm proxyId={proxyId}/>
        </FormContainer>
    )
};

export default ProxyFormContainer;
