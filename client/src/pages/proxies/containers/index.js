import ProxiesContainer from "./ProxiesContainer";
import ProxyFormContainer from "./ProxyFormContainer";

export {
    ProxyFormContainer,
    ProxiesContainer
}
