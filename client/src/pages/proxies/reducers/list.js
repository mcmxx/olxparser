import {
    DELETE_PROXY,
    GET_PROXIES,
    SWITCH_PROXY,
    LOADING,
    LOADED
} from "../../../constants/actions";

const initialState = {
    items: [],
    loading: true
};

export default function list(state = initialState, action) {
    const {
        type,
        payload
    } = action;

    if(action.type === LOADING) {
        return {
            ...state,
            loading: true
        }
    }

    if(action.type === LOADED) {
        return {
            ...state,
            loading: false
        }
    }

    if(type === GET_PROXIES) {
        return payload;
    }

    if(type === SWITCH_PROXY) {
        const newItems = state.items.map((item) => {
            if(item._id === payload.id) {
                item.active = !item.active;
            }
            return item;
        })
        return {
            items: newItems
        }
    }

    if(type === DELETE_PROXY) {
        const newItems = state.items.filter((item) => {
            return item._id !== payload.id;
        });
        return {
            items: newItems
        }
    }

    return state;
}