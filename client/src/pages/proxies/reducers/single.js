import {
    GET_PROXY,
    CREATE_PROXY,
    UPDATE_PROXY,
    RESET_PROXY_FORM
} from "../../../constants/actions";

const initialState = {
    t: Date.now(),
    error: null,
    loaded: false
};

export default function single(state = initialState, action) {
    switch (action.type) {
        case GET_PROXY:
            return {
                ...state,
                ...action.payload.item,
                t: Date.now()
            }

        case CREATE_PROXY:
        case UPDATE_PROXY:
            if(!action.payload) {
                return {
                    ...state,
                    error: true,
                    loaded: false
                }
            } else {
                return {
                    ...state,
                    error: false,
                    loaded: true
                }
            }

        case RESET_PROXY_FORM:
        default:
            return initialState;
    }
}
